﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Outlook;
using System.Text.RegularExpressions;
using System.IO;

namespace OutlookAddIn1.Utiles
{
    class cUtil
    {
        /// <summary>
        /// Funció que transforma una expresion en su equivalente para que la acepte la salida "ToString()" del DateTime
        /// </summary>
        /// <param name="expresion">Expresion a transofrmar</param>
        /// <returns>Expresion transformada</returns>
        public string TransformarExpresion(string expresion)
        {
            string posiciones = "0";
            int posicionPrincipio = 0;

            if (expresion.ToUpper().Contains("#WINUSER#")) expresion = expresion.Replace("#WINUSER#", Environment.UserName);
            if (expresion.ToUpper().Contains("#MAILUSER#")) expresion = expresion.Replace("#MAILUSER#", Globals.ThisAddIn.Application.Session.CurrentUser.Name);

            while (posicionPrincipio < expresion.Length - 1)
            {
                if (expresion.IndexOf("<", posicionPrincipio) > -1 && expresion.IndexOf(">", expresion.IndexOf("<", posicionPrincipio)) > -1)
                {
                    posicionPrincipio = expresion.IndexOf("<", posicionPrincipio) - 1;
                    expresion = expresion.Remove(posicionPrincipio + 1, 1);
                    posiciones += "," + posicionPrincipio;
                    posicionPrincipio = expresion.IndexOf(">", posicionPrincipio);
                    expresion = expresion.Remove(posicionPrincipio, 1);
                    posiciones += "," + posicionPrincipio;
                }
                if (!(expresion.IndexOf("<", posicionPrincipio) > -1 && expresion.IndexOf(">", expresion.IndexOf("<", posicionPrincipio)) > -1))
                {
                    posicionPrincipio = expresion.Length - 1;
                    posiciones += "," + posicionPrincipio;
                }
            }
            if (posiciones == "0") posiciones = "";

            string regexSearch = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
            Regex r = new Regex(string.Format("[{0}]", Regex.Escape(regexSearch)));
            expresion = r.Replace(expresion, "_");

            AnyadirCaracteresEscape(ref expresion, posiciones);
            if (expresion.Length == 1) expresion = @"\" + expresion;

            return expresion;
        }

        /// <summary>
        /// Función que añade caracteres de escape ("\") entre las posiciones pasadas por parametro.
        /// </summary>
        /// <param name="cadena">Cadena donde añadir los caracteres de escape</param>
        /// <param name="posiciones">Posiciones en las que añadir los caracteres de escape, separadas por comas, por pares (inicio, fin, inicio, fin...) de la manera 0,2,4,8</param>
        public void AnyadirCaracteresEscape(ref string cadena, string posiciones)
        {
            if (posiciones != "")
            {
                string[] posicion = posiciones.Split(',');
                int contadorPosiciones = posicion.Length - 1;
                while (contadorPosiciones >= 0)
                {
                    for (int i = Convert.ToInt32(posicion[contadorPosiciones]); i >= Convert.ToInt32(posicion[contadorPosiciones - 1]); i--)
                        cadena = cadena.Insert(i, @"\");
                    contadorPosiciones = contadorPosiciones - 2;
                }
            }
        }

        public string CrearNombre(DateTime dat)
        {
            string nombreArchivo = "";
            if (dat != null)
            {
                nombreArchivo = dat.ToString(TransformarExpresion(Preferencias.cPreferencias.instancia.nombrePST));
            }
            return nombreArchivo;
        }

        public void AnyadirCarpeta(ref List<KeyValuePair<Store, List<MAPIFolder>>> lista, MAPIFolder carpeta)
        {
            int posicion = lista.FindIndex(d => d.Key.StoreID == carpeta.StoreID);
            if (posicion >= 0)
            {
                if (!lista[posicion].Value.Exists(d => d.EntryID == carpeta.EntryID))
                    lista[posicion].Value.Add(carpeta);
            }
            else
                lista.Add(new KeyValuePair<Store, List<MAPIFolder>>(carpeta.Store, new List<MAPIFolder>() { carpeta }));
        }

        public void EliminarFicherosInexistentes(ref List<string> listaFicheros)
        {
            int contador = listaFicheros.Count - 1;
            while (contador >= 0)
            {
                if (!File.Exists(listaFicheros[contador]))
                    listaFicheros.RemoveAt(contador);

                contador--;
            }
        }
    }
}
