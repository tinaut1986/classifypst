﻿using Microsoft.Office.Interop.Outlook;
using OutlookAddIn1.Extension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OutlookAddIn1.Utiles
{
    public class cElementoOutlook
    {
        object elemento;

        public cElementoOutlook(object obj)
        {
            elemento = obj;
        }

        public DateTime Fecha()
        {
            if (elemento is TaskItem)
                return ((TaskItem)elemento).FechaArchivado();
            else if (elemento is TaskRequestItem)
                return ((TaskRequestItem)elemento).FechaArchivado();
            else if (elemento is TaskRequestUpdateItem)
                return ((TaskRequestUpdateItem)elemento).FechaArchivado();
            else if (elemento is TaskRequestAcceptItem)
                return ((TaskRequestAcceptItem)elemento).FechaArchivado();
            else if (elemento is TaskRequestDeclineItem)
                return ((TaskRequestDeclineItem)elemento).FechaArchivado();
            else if (elemento is AppointmentItem)
                return ((AppointmentItem)elemento).FechaArchivado();
            else if (elemento is MeetingItem)
                return ((MeetingItem)elemento).FechaArchivado();
            else if (elemento is ContactItem)
                return ((ContactItem)elemento).FechaArchivado();
            else if (elemento is PostItem)
                return ((PostItem)elemento).FechaArchivado();
            else if (elemento is NoteItem)
                return ((NoteItem)elemento).FechaArchivado();
            else if (elemento is ReportItem)
                return ((ReportItem)elemento).FechaArchivado();
            else if (elemento is JournalItem)
                return ((JournalItem)elemento).FechaArchivado();
            else if (elemento is MailItem)
                return ((MailItem)elemento).FechaArchivado();
            else
                throw new SystemException("El elemento no tiene ningun tipo de carpeta asociado");
        }

        public OlDefaultFolders TipoCarpeta()
        {
            if (elemento is TaskItem)
                return ((TaskItem)elemento).TipoCarpeta();
            else if (elemento is TaskRequestItem)
                return ((TaskRequestItem)elemento).TipoCarpeta();
            else if (elemento is TaskRequestUpdateItem)
                return ((TaskRequestUpdateItem)elemento).TipoCarpeta();
            else if (elemento is TaskRequestAcceptItem)
                return ((TaskRequestAcceptItem)elemento).TipoCarpeta();
            else if (elemento is TaskRequestDeclineItem)
                return ((TaskRequestDeclineItem)elemento).TipoCarpeta();
            else if (elemento is AppointmentItem)
                return ((AppointmentItem)elemento).TipoCarpeta();
            else if (elemento is MeetingItem)
                return ((MeetingItem)elemento).TipoCarpeta();
            else if (elemento is ContactItem)
                return ((ContactItem)elemento).TipoCarpeta();
            else if (elemento is PostItem)
                return ((PostItem)elemento).TipoCarpeta();
            else if (elemento is NoteItem)
                return ((NoteItem)elemento).TipoCarpeta();
            else if (elemento is ReportItem)
                return ((ReportItem)elemento).TipoCarpeta();
            else if (elemento is JournalItem)
                return ((JournalItem)elemento).TipoCarpeta();
            else if (elemento is MailItem)
                return ((MailItem)elemento).TipoCarpeta();
            else
                throw new SystemException("El elemento no tiene ningun tipo de carpeta asociado");
        }

        public bool Archivar()
        {
            System.Windows.Forms.Application.DoEvents();
            int contador = 0;
            bool resultado = false;

            /*Si el PST donde tiene que archivarse no existe, al crearlo se "desasocia" el elemento a archivar con su almacen, con lo que salta una excepcion.
            En estos casos, hace falta intentar archivarlo dos veces*/
            while (resultado == false && contador < 2)
            {
                try
                {
                    if (elemento is TaskItem)
                        resultado = ((TaskItem)elemento).Archivar();
                    else if (elemento is TaskRequestItem)
                        return ((TaskRequestItem)elemento).Archivar();
                    else if (elemento is TaskRequestUpdateItem)
                        return ((TaskRequestUpdateItem)elemento).Archivar();
                    else if (elemento is TaskRequestAcceptItem)
                        return ((TaskRequestAcceptItem)elemento).Archivar();
                    else if (elemento is TaskRequestDeclineItem)
                        return ((TaskRequestDeclineItem)elemento).Archivar();
                    else if (elemento is AppointmentItem)
                        resultado = ((AppointmentItem)elemento).Archivar();
                    else if (elemento is MeetingItem)
                        resultado = ((MeetingItem)elemento).Archivar();
                    else if (elemento is ContactItem)
                        resultado = ((ContactItem)elemento).Archivar();
                    else if (elemento is PostItem)
                        resultado = ((PostItem)elemento).Archivar();
                    else if (elemento is NoteItem)
                        resultado = ((NoteItem)elemento).Archivar();
                    else if (elemento is ReportItem)
                        resultado = ((ReportItem)elemento).Archivar();
                    else if (elemento is JournalItem)
                        resultado = ((JournalItem)elemento).Archivar();
                    else if (elemento is SharingItem)
                        resultado = ((SharingItem)elemento).Archivar();
                    else if (elemento is MailItem)
                        resultado = ((MailItem)elemento).Archivar();
                    else
                        throw new System.Exception("El elemento no se puede archivar");
                }
                catch (System.Exception ex)
                {
                    if (contador < 2)
                        contador++;
                    else
                        throw ex;
                }
            }
            return resultado;
        }

        public bool Eliminar()
        {
            System.Windows.Forms.Application.DoEvents();

            if (elemento is TaskItem)
                ((TaskItem)elemento).Eliminar();
            else if (elemento is TaskRequestItem)
                ((TaskRequestItem)elemento).Eliminar();
            else if (elemento is TaskRequestUpdateItem)
                ((TaskRequestUpdateItem)elemento).Eliminar();
            else if (elemento is TaskRequestAcceptItem)
                ((TaskRequestAcceptItem)elemento).Eliminar();
            else if (elemento is TaskRequestDeclineItem)
                ((TaskRequestDeclineItem)elemento).Eliminar();
            else if (elemento is AppointmentItem)
                ((AppointmentItem)elemento).Eliminar();
            else if (elemento is MeetingItem)
                ((MeetingItem)elemento).Eliminar();
            else if (elemento is ContactItem)
                ((ContactItem)elemento).Eliminar();
            else if (elemento is PostItem)
                ((PostItem)elemento).Eliminar();
            else if (elemento is NoteItem)
                ((NoteItem)elemento).Eliminar();
            else if (elemento is ReportItem)
                ((ReportItem)elemento).Eliminar();
            else if (elemento is JournalItem)
                ((JournalItem)elemento).Eliminar();
            else if (elemento is SharingItem)
                ((SharingItem)elemento).Eliminar();
            else if (elemento is MailItem)
                ((MailItem)elemento).Eliminar();
            else
                throw new System.Exception("El elemento no se puede eliminar.");

            return true;
        }

        public bool EstaEnPapelera()
        {
            if (elemento is TaskItem)
                return ((TaskItem)elemento).EstaEnPapelera();
            else if (elemento is TaskRequestItem)
                return ((TaskRequestItem)elemento).EstaEnPapelera();
            else if (elemento is TaskRequestUpdateItem)
                return ((TaskRequestUpdateItem)elemento).EstaEnPapelera();
            else if (elemento is TaskRequestAcceptItem)
                return ((TaskRequestAcceptItem)elemento).EstaEnPapelera();
            else if (elemento is TaskRequestDeclineItem)
                return ((TaskRequestDeclineItem)elemento).EstaEnPapelera();
            else if (elemento is AppointmentItem)
                return ((AppointmentItem)elemento).EstaEnPapelera();
            else if (elemento is MeetingItem)
                return ((MeetingItem)elemento).EstaEnPapelera();
            else if (elemento is ContactItem)
                return ((ContactItem)elemento).EstaEnPapelera();
            else if (elemento is PostItem)
                return ((PostItem)elemento).EstaEnPapelera();
            else if (elemento is NoteItem)
                return ((NoteItem)elemento).EstaEnPapelera();
            else if (elemento is ReportItem)
                return ((ReportItem)elemento).EstaEnPapelera();
            else if (elemento is JournalItem)
                return ((JournalItem)elemento).EstaEnPapelera();
            else if (elemento is SharingItem)
                return ((SharingItem)elemento).EstaEnPapelera();
            else if (elemento is MailItem)
                return ((MailItem)elemento).EstaEnPapelera();
            else
                throw new System.Exception("El elemento no se puede comprobar.");
        }

        public void Mover(MAPIFolder destino)
        {
            System.Windows.Forms.Application.DoEvents();

            if (elemento is TaskItem)
                ((TaskItem)elemento).Move(destino);
            else if (elemento is TaskRequestItem)
                ((TaskRequestItem)elemento).Move(destino);
            else if (elemento is TaskRequestUpdateItem)
                ((TaskRequestUpdateItem)elemento).Move(destino);
            else if (elemento is TaskRequestAcceptItem)
                ((TaskRequestAcceptItem)elemento).Move(destino);
            else if (elemento is TaskRequestDeclineItem)
                ((TaskRequestDeclineItem)elemento).Move(destino);
            else if (elemento is AppointmentItem)
                ((AppointmentItem)elemento).Move(destino);
            else if (elemento is MeetingItem)
                ((MeetingItem)elemento).Move(destino);
            else if (elemento is ContactItem)
                ((ContactItem)elemento).Move(destino);
            else if (elemento is PostItem)
                ((PostItem)elemento).Move(destino);
            else if (elemento is NoteItem)
            {
                ((NoteItem)elemento).Move(destino);
                Eliminar();
            }
            else if (elemento is ReportItem)
                ((ReportItem)elemento).Move(destino);
            else if (elemento is JournalItem)
                ((JournalItem)elemento).Move(destino);
            else if (elemento is SharingItem)
                ((SharingItem)elemento).Move(destino);
            else if (elemento is MailItem)
                ((MailItem)elemento).Move(destino);
            else
                throw new SystemException("El elemento no tiene ningun tipo de carpeta asociado");
        }
    }
}
