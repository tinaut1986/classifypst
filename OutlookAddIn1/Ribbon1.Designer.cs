﻿namespace OutlookAddIn1
{
    partial class ribonClassify : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public ribonClassify()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tab1 = this.Factory.CreateRibbonTab();
            this.grpArchivar = this.Factory.CreateRibbonGroup();
            this.button1 = this.Factory.CreateRibbonButton();
            this.btnArchivar = this.Factory.CreateRibbonButton();
            this.grpConfiguracion = this.Factory.CreateRibbonGroup();
            this.ribbonPreferencias = this.Factory.CreateRibbonButton();
            this.grpUtilidades = this.Factory.CreateRibbonGroup();
            this.btnQuitarPSTsVacios = this.Factory.CreateRibbonButton();
            this.btnEliminarCarpetasVacias = this.Factory.CreateRibbonButton();
            this.btnEliminarPSTs = this.Factory.CreateRibbonButton();
            this.grpOtros = this.Factory.CreateRibbonGroup();
            this.btnInfo = this.Factory.CreateRibbonButton();
            this.tmrEliminarPSTSPendientes = new System.Windows.Forms.Timer(this.components);
            this.fdialAnyadirPSTs = new System.Windows.Forms.OpenFileDialog();
            this.tab1.SuspendLayout();
            this.grpArchivar.SuspendLayout();
            this.grpConfiguracion.SuspendLayout();
            this.grpUtilidades.SuspendLayout();
            this.grpOtros.SuspendLayout();
            this.SuspendLayout();
            // 
            // tab1
            // 
            this.tab1.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office;
            this.tab1.Groups.Add(this.grpArchivar);
            this.tab1.Groups.Add(this.grpConfiguracion);
            this.tab1.Groups.Add(this.grpUtilidades);
            this.tab1.Groups.Add(this.grpOtros);
            this.tab1.Label = "ClassifyPST";
            this.tab1.Name = "tab1";
            // 
            // grpArchivar
            // 
            this.grpArchivar.Items.Add(this.button1);
            this.grpArchivar.Items.Add(this.btnArchivar);
            this.grpArchivar.Label = "Archivo";
            this.grpArchivar.Name = "grpArchivar";
            // 
            // button1
            // 
            this.button1.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.button1.Image = global::OutlookAddIn1.Properties.Resources.Mas;
            this.button1.Label = "Añadir PSTs";
            this.button1.Name = "button1";
            this.button1.ShowImage = true;
            this.button1.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.button1_Click);
            // 
            // btnArchivar
            // 
            this.btnArchivar.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btnArchivar.Image = global::OutlookAddIn1.Properties.Resources.Carpeta_Abierta;
            this.btnArchivar.Label = "Archivar Correos";
            this.btnArchivar.Name = "btnArchivar";
            this.btnArchivar.ShowImage = true;
            this.btnArchivar.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnArchivar_Click);
            // 
            // grpConfiguracion
            // 
            this.grpConfiguracion.Items.Add(this.ribbonPreferencias);
            this.grpConfiguracion.Label = "Configuración";
            this.grpConfiguracion.Name = "grpConfiguracion";
            // 
            // ribbonPreferencias
            // 
            this.ribbonPreferencias.Image = global::OutlookAddIn1.Properties.Resources.Configuracion;
            this.ribbonPreferencias.Label = "Preferencias";
            this.ribbonPreferencias.Name = "ribbonPreferencias";
            this.ribbonPreferencias.ShowImage = true;
            this.ribbonPreferencias.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.ribbonPreferencias_Click);
            // 
            // grpUtilidades
            // 
            this.grpUtilidades.Items.Add(this.btnQuitarPSTsVacios);
            this.grpUtilidades.Items.Add(this.btnEliminarCarpetasVacias);
            this.grpUtilidades.Items.Add(this.btnEliminarPSTs);
            this.grpUtilidades.Label = "Utilidades";
            this.grpUtilidades.Name = "grpUtilidades";
            // 
            // btnQuitarPSTsVacios
            // 
            this.btnQuitarPSTsVacios.Image = global::OutlookAddIn1.Properties.Resources.Menos;
            this.btnQuitarPSTsVacios.Label = "Quitar y eliminar PSTs vacios";
            this.btnQuitarPSTsVacios.Name = "btnQuitarPSTsVacios";
            this.btnQuitarPSTsVacios.ShowImage = true;
            this.btnQuitarPSTsVacios.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnQuitarPSTsVacios_Click);
            // 
            // btnEliminarCarpetasVacias
            // 
            this.btnEliminarCarpetasVacias.Image = global::OutlookAddIn1.Properties.Resources.Papelera_vacia;
            this.btnEliminarCarpetasVacias.Label = "Eliminar carpetas vacias";
            this.btnEliminarCarpetasVacias.Name = "btnEliminarCarpetasVacias";
            this.btnEliminarCarpetasVacias.ShowImage = true;
            this.btnEliminarCarpetasVacias.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnEliminarCarpetasVacias_Click);
            // 
            // btnEliminarPSTs
            // 
            this.btnEliminarPSTs.Image = global::OutlookAddIn1.Properties.Resources.Hora;
            this.btnEliminarPSTs.Label = "NO intentando eliminar PSTs pendientes";
            this.btnEliminarPSTs.Name = "btnEliminarPSTs";
            this.btnEliminarPSTs.ShowImage = true;
            this.btnEliminarPSTs.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnEliminarPSTs_Click);
            // 
            // grpOtros
            // 
            this.grpOtros.Items.Add(this.btnInfo);
            this.grpOtros.Label = "Otros";
            this.grpOtros.Name = "grpOtros";
            // 
            // btnInfo
            // 
            this.btnInfo.Image = global::OutlookAddIn1.Properties.Resources.Info;
            this.btnInfo.Label = "Información";
            this.btnInfo.Name = "btnInfo";
            this.btnInfo.ShowImage = true;
            this.btnInfo.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnInfo_Click);
            // 
            // tmrEliminarPSTSPendientes
            // 
            this.tmrEliminarPSTSPendientes.Tick += new System.EventHandler(this.tmrEliminarPSTSPendientes_Tick);
            // 
            // fdialAnyadirPSTs
            // 
            this.fdialAnyadirPSTs.FileName = "Archivo.pst";
            this.fdialAnyadirPSTs.Filter = "Almacenes de Outlook|*.pst|Todos los archiovs|*.*";
            this.fdialAnyadirPSTs.Multiselect = true;
            // 
            // ribonClassify
            // 
            this.Name = "ribonClassify";
            this.RibbonType = "Microsoft.Outlook.Explorer";
            this.Tabs.Add(this.tab1);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.Ribbon1_Load);
            this.tab1.ResumeLayout(false);
            this.tab1.PerformLayout();
            this.grpArchivar.ResumeLayout(false);
            this.grpArchivar.PerformLayout();
            this.grpConfiguracion.ResumeLayout(false);
            this.grpConfiguracion.PerformLayout();
            this.grpUtilidades.ResumeLayout(false);
            this.grpUtilidades.PerformLayout();
            this.grpOtros.ResumeLayout(false);
            this.grpOtros.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab tab1;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup grpConfiguracion;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton ribbonPreferencias;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup grpArchivar;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnArchivar;
        public System.Windows.Forms.Timer tmrEliminarPSTSPendientes;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton button1;
        private System.Windows.Forms.OpenFileDialog fdialAnyadirPSTs;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup grpUtilidades;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnQuitarPSTsVacios;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnEliminarPSTs;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnEliminarCarpetasVacias;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup grpOtros;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnInfo;
    }

    partial class ThisRibbonCollection
    {
        internal ribonClassify Ribbon1
        {
            get { return this.GetRibbon<ribonClassify>(); }
        }
    }
}
