﻿/*    Copyright (C) 2015 - Tinaut1986

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OutlookAddIn1
{
    public partial class frmFechas : Form
    {
        public frmFechas()
        {
            InitializeComponent();
        }

        private void frmFechas_Load(object sender, EventArgs e)
        {
            dateDesde.Value = new DateTime(DateTime.Now.Year - 5, 1, 1, 0, 0, 0);
            dateDesde.Checked = false;
            dateHasta.Value = new DateTime(DateTime.Now.Year, 1, 1, 0, 0, 0).AddSeconds(-1);
        }
    }
}
