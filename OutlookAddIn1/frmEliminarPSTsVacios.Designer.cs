﻿namespace OutlookAddIn1
{
    partial class frmEliminarPSTsVacios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvPSTs = new System.Windows.Forms.DataGridView();
            this.Ruta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.btnAceptar = new System.Windows.Forms.Button();
            this.btnDeshacer = new System.Windows.Forms.Button();
            this.btnEstadoCronometro = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPSTs)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvPSTs
            // 
            this.dgvPSTs.AllowUserToAddRows = false;
            this.dgvPSTs.AllowUserToResizeRows = false;
            this.dgvPSTs.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvPSTs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPSTs.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Ruta});
            this.dgvPSTs.Location = new System.Drawing.Point(12, 75);
            this.dgvPSTs.Name = "dgvPSTs";
            this.dgvPSTs.Size = new System.Drawing.Size(494, 236);
            this.dgvPSTs.TabIndex = 0;
            this.dgvPSTs.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dgvPSTs_UserDeletingRow);
            // 
            // Ruta
            // 
            this.Ruta.HeaderText = "Ruta";
            this.Ruta.Name = "Ruta";
            this.Ruta.ReadOnly = true;
            this.Ruta.Width = 55;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(38, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(440, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Los siguientes PSTs estan pendientes de eliminar. ¿Desea continuar intentando eli" +
    "minarlos?";
            // 
            // btnAceptar
            // 
            this.btnAceptar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnAceptar.Location = new System.Drawing.Point(431, 317);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(75, 23);
            this.btnAceptar.TabIndex = 2;
            this.btnAceptar.Text = "Aceptar";
            this.btnAceptar.UseVisualStyleBackColor = true;
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // btnDeshacer
            // 
            this.btnDeshacer.Location = new System.Drawing.Point(12, 317);
            this.btnDeshacer.Name = "btnDeshacer";
            this.btnDeshacer.Size = new System.Drawing.Size(75, 23);
            this.btnDeshacer.TabIndex = 2;
            this.btnDeshacer.Text = "Deshacer";
            this.btnDeshacer.UseVisualStyleBackColor = true;
            this.btnDeshacer.Click += new System.EventHandler(this.btnDeshacer_Click);
            // 
            // btnEstadoCronometro
            // 
            this.btnEstadoCronometro.Location = new System.Drawing.Point(431, 12);
            this.btnEstadoCronometro.Name = "btnEstadoCronometro";
            this.btnEstadoCronometro.Size = new System.Drawing.Size(75, 23);
            this.btnEstadoCronometro.TabIndex = 3;
            this.btnEstadoCronometro.Text = "No";
            this.btnEstadoCronometro.UseVisualStyleBackColor = true;
            this.btnEstadoCronometro.Click += new System.EventHandler(this.btnEstadoCronometro_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(300, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Intentar eliminar PSTs";
            // 
            // frmEliminarPSTsVacios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(518, 354);
            this.Controls.Add(this.btnEstadoCronometro);
            this.Controls.Add(this.btnDeshacer);
            this.Controls.Add(this.btnAceptar);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgvPSTs);
            this.Name = "frmEliminarPSTsVacios";
            this.Text = "frmEliminarPSTsVacios";
            this.Load += new System.EventHandler(this.frmEliminarPSTsVacios_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPSTs)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvPSTs;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnAceptar;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ruta;
        private System.Windows.Forms.Button btnDeshacer;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.Button btnEstadoCronometro;
    }
}