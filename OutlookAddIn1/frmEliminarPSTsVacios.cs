﻿/*    Copyright (C) 2015 - Tinaut1986

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OutlookAddIn1
{
    public partial class frmEliminarPSTsVacios : Form
    {
        List<string> lista;
        Timer crono;
        public frmEliminarPSTsVacios(ref List<string> l, Timer c)
        {
            InitializeComponent();
            lista = l;
            crono = c;
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dgvPSTs.Rows)
            {
                if (!row.Visible)
                    lista.Remove(row.Cells["Ruta"].Value.ToString());
            }
        }

        private void frmEliminarPSTsVacios_Load(object sender, EventArgs e)
        {
            if (crono.Enabled)
            {
                btnEstadoCronometro.Text = "Si";
                btnEstadoCronometro.ForeColor = Color.Green;
            }
            else
            {
                btnEstadoCronometro.Text = "No";
                btnEstadoCronometro.ForeColor = Color.Red;
            }

            int contador = lista.Count - 1;
            while (contador >= 0)
            {
                if (File.Exists(lista[contador]))
                    dgvPSTs.Rows.Add(lista[contador]);
                else
                    lista.RemoveAt(contador);
                contador--;
            }
        }

        private void dgvPSTs_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            e.Row.Visible = false;
            e.Cancel = true;
        }

        private void btnDeshacer_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow fila in dgvPSTs.Rows)
                if (!fila.Visible)
                    fila.Visible = true;
        }

        private void btnEstadoCronometro_Click(object sender, EventArgs e)
        {
            btnEstadoCronometro.Text = (btnEstadoCronometro.Text == "Si" ? "No" : "Si");
            btnEstadoCronometro.ForeColor = (btnEstadoCronometro.Text == "Si" ? Color.Green : Color.Red);
        }
    }
}
