﻿/*    Copyright (C) 2015 - Tinaut1986

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

using Microsoft.Office.Interop.Outlook;
using OutlookAddIn1.Extension;
using OutlookAddIn1.Utiles;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OutlookAddIn1
{
    public partial class frmPrincipal : Form
    {
        private static frmPrincipal instancia = null;

        NameSpace ns = Globals.ThisAddIn.Application.Session;
        Utiles.cUtil util = new Utiles.cUtil();
        MAPIFolder carpetaActual;
        public bool detener = false;

        private frmPrincipal() { InitializeComponent(); }

        public static frmPrincipal Instancia()
        {
            if (instancia == null)
                instancia = new frmPrincipal();
            return instancia;
        }

        public void CargarPSTs()
        {
            DeshabilitarAcciones(true);
            treePSTs.Nodes.Clear();
            treePSTs.Sorted = true;
            foreach (Store PST in ns.Stores)
            {
                int nElementos = 0;

                treePSTs.Nodes.Add(MontarSubNodos(PST.GetRootFolder(), ref nElementos));
                System.Windows.Forms.Application.DoEvents();
            }
            treePSTs.BackColor = treePSTs.BackColor;
            HabilitarAcciones();
        }

        public TreeNode MontarSubNodos(MAPIFolder carpeta, ref int nElementos)
        {
            TreeNode nodo = new TreeNode();
            int nSubelementos = carpeta.Items.Count;

            foreach (MAPIFolder subCarpeta in carpeta.Folders)
                nodo.Nodes.Add(MontarSubNodos(subCarpeta, ref nSubelementos));

            nElementos += nSubelementos;
            nodo.Text = carpeta.Name + " - (" + carpeta.Items.Count + ") - (" + nSubelementos + ")";
            nodo.Name = carpeta.EntryID;
            nodo.Tag = carpeta;
            if (carpeta.Parent is NameSpace)
                nodo.NodeFont = new Font(treePSTs.Font, FontStyle.Bold);

            return nodo;
        }

        private void frmPrincipal_Load(object sender, EventArgs e)
        {
            txtNombreArchivo.Text = Preferencias.cPreferencias.instancia.nombrePST;
            tmrCargarArbol.Enabled = true;
            tmrCargarArbol.Start();
        }

        private void tmrCargarArbol_Tick(object sender, EventArgs e)
        {
            tmrCargarArbol.Enabled = false;
            CargarPSTs();
        }

        private void todoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<KeyValuePair<Store, List<MAPIFolder>>> lista = new List<KeyValuePair<Store, List<MAPIFolder>>>();
            foreach (TreeNode nodo in treePSTs.Nodes)
                GuardarSubNodos(nodo, true, ref lista);

            Preferencias.cPreferencias.instancia.archivarFechaInicio = false;
            Preferencias.cPreferencias.instancia.archivarFechaFin = false;
            ArchivarCarpetas(ref lista);
        }

        private void GuardarSubNodos(TreeNode nodo, bool marcats, ref List<KeyValuePair<Store, List<MAPIFolder>>> lista)
        {
            foreach (TreeNode subNodo in nodo.Nodes)
                GuardarSubNodos(subNodo, marcats, ref lista);

            if (!marcats || (marcats && nodo.Checked))
                util.AnyadirCarpeta(ref lista, (MAPIFolder)nodo.Tag);
        }

        private void ArchivarCarpetas(ref List<KeyValuePair<Store, List<MAPIFolder>>> lista)
        {
            DialogResult resultado = MessageBox.Show("Seguro que quiere archivar los correos en PSTs con el formato de nombre '" + lblNombreArchivo.Text + "'?",
                "Archivar PSTs con el formato de nombre '" + lblNombreArchivo.Text + "'?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (resultado == DialogResult.Yes)
            {
                DeshabilitarAcciones(true);
                sttContador.Visible = true;
                sttDetener.Visible = true;
                sttProgress.Visible = true;
                int contadorPar = lista.Count - 1;
                while (contadorPar >= 0 && !detener)
                {
                    int contadorCarpeta = lista[contadorPar].Value.Count - 1;
                    while (contadorCarpeta >= 0 && !detener)
                    {
                        try
                        {
                            carpetaActual = lista[contadorPar].Value[contadorCarpeta];
                            sttProgress.Maximum = carpetaActual.Items.Count;
                            tmrActualizarUI.Start();
                            carpetaActual.Archivar();
                        }
                        catch (System.Exception ex)
                        {
                            Console.Write(ex.Message);
                        }
                        contadorCarpeta--;
                    }

                    if (Preferencias.cPreferencias.instancia.eliminarPSTsVaciosTrasArchivar)
                        lista[contadorPar].Key.QuitarYEliminarSiVacio(false);
                    contadorPar--;
                }

                sttContador.Visible = false;
                sttDetener.Visible = false;
                sttProgress.Visible = false;
                CargarPSTs();
                System.Windows.Forms.Application.DoEvents();
                Preferencias.cPreferencias.instancia.detener = false;
                HabilitarAcciones();
            }
        }

        private void todasToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            List<KeyValuePair<Store, List<MAPIFolder>>> lista = new List<KeyValuePair<Store, List<MAPIFolder>>>();
            foreach (TreeNode nodo in treePSTs.Nodes)
                GuardarSubNodos(nodo, false, ref lista);

            Preferencias.cPreferencias.instancia.archivarFechaInicio = false;
            Preferencias.cPreferencias.instancia.archivarFechaFin = false;
            ArchivarCarpetas(ref lista);
        }

        private void entreFechasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmFechas fechas = new frmFechas();
            DialogResult respuesta = fechas.ShowDialog();
            if (respuesta == DialogResult.OK)
            {
                Preferencias.cPreferencias.instancia.archivarFechaInicio = fechas.dateDesde.Checked;
                Preferencias.cPreferencias.instancia.fechaInicio = fechas.dateDesde.Value;
                Preferencias.cPreferencias.instancia.archivarFechaFin = fechas.dateHasta.Checked;
                Preferencias.cPreferencias.instancia.fechaFin = fechas.dateHasta.Value;

                List<KeyValuePair<Store, List<MAPIFolder>>> lista = new List<KeyValuePair<Store, List<MAPIFolder>>>();
                foreach (TreeNode nodo in treePSTs.Nodes)
                    GuardarSubNodos(nodo, false, ref lista);

                ArchivarCarpetas(ref lista);
            }
        }

        private void entreFechasToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmFechas fechas = new frmFechas();
            DialogResult respuesta = fechas.ShowDialog();
            if (respuesta == DialogResult.OK)
            {
                Preferencias.cPreferencias.instancia.archivarFechaInicio = fechas.dateDesde.Checked;
                Preferencias.cPreferencias.instancia.fechaInicio = fechas.dateDesde.Value;
                Preferencias.cPreferencias.instancia.archivarFechaFin = fechas.dateHasta.Checked;
                Preferencias.cPreferencias.instancia.fechaFin = fechas.dateHasta.Value;

                List<KeyValuePair<Store, List<MAPIFolder>>> lista = new List<KeyValuePair<Store, List<MAPIFolder>>>();
                foreach (TreeNode nodo in treePSTs.Nodes)
                    GuardarSubNodos(nodo, true, ref lista);

                ArchivarCarpetas(ref lista);
            }
        }

        private void ActualizarUI()
        {
            try
            {
                sttProgress.Value = sttProgress.Maximum - Extension.cExtensionCarpeta.elementoActual;
                sttContador.Text = sttProgress.Value + "/" + sttProgress.Maximum + " - " + carpetaActual.Store.GetRootFolder().Name + " / " + carpetaActual.Name;
            }
            catch (System.Exception ex)
            {

            }
        }

        private void tmrActualizarUI_Tick(object sender, EventArgs e)
        {
            tmrActualizarUI.Enabled = false;
            ActualizarUI();
            tmrActualizarUI.Enabled = true;
        }

        private void añadirPSTsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (fdialAnyadirPSTs.ShowDialog() == DialogResult.OK)
                foreach (string str in fdialAnyadirPSTs.FileNames)
                    ns.AnyadirPST(str);
            CargarPSTs();
        }

        private void vaciosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DeshabilitarAcciones(true);
            ns.EliminarPSTsVacios();
            CargarPSTs();
            HabilitarAcciones();
        }

        private void MarcarSubNodos(TreeNode nodo, bool marcar)
        {
            nodo.Checked = marcar;
            foreach (TreeNode subNodo in nodo.Nodes)
                MarcarSubNodos(subNodo, marcar);
        }

        private void treePSTs_AfterCheck(object sender, TreeViewEventArgs e)
        {
            treePSTs.AfterCheck -= treePSTs_AfterCheck;
            if (Form.ModifierKeys == Keys.Control)
                MarcarSubNodos(e.Node, e.Node.Checked);
            treePSTs.AfterCheck += treePSTs_AfterCheck;
        }

        private void todasToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            DeshabilitarAcciones(true);
            ns.EliminarCarpetasVacias();
            CargarPSTs();
            HabilitarAcciones();
        }

        private void seleccionadasToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            DeshabilitarAcciones(true);
            List<KeyValuePair<Store, List<MAPIFolder>>> lista = new List<KeyValuePair<Store, List<MAPIFolder>>>();
            foreach (TreeNode nodo in treePSTs.Nodes)
                GuardarSubNodos(nodo, true, ref lista);

            ns.EliminarCarpetasVacias(ref lista);
            CargarPSTs();
            HabilitarAcciones();
        }

        private void DeshabilitarAcciones(bool bloquearTexto)
        {
            archivoToolStripMenuItem.Enabled = false;
            utilidadesToolStripMenuItem.Enabled = false;
            if (bloquearTexto)
                txtNombreArchivo.Enabled = false;
        }

        private void HabilitarAcciones()
        {
            archivoToolStripMenuItem.Enabled = true;
            utilidadesToolStripMenuItem.Enabled = true;
            txtNombreArchivo.Enabled = true;
        }

        private void EliminarDiarios(ref List<Store> lista, DateTime fechaInicio, bool inicio, DateTime fechaFin, bool fin)
        {
            DeshabilitarAcciones(true);
            sttContador.Visible = true;
            sttDetener.Visible = true;
            sttProgress.Visible = true;
            foreach (Store store in lista)
            {
                MAPIFolder diario = store.GetDefaultFolder(OlDefaultFolders.olFolderJournal);
                if (diario.Items.Count > 0 && !detener)
                {
                    try
                    {
                        carpetaActual = diario;
                        MAPIFolder diarioEliminado = diario.CrearSubCarpeta(carpetaActual.Name + " - Eliminado", OlDefaultFolders.olFolderJournal, true);

                        sttProgress.Maximum = diario.Items.Count;
                        tmrActualizarUI.Start();
                        diario.MoverElementos(diarioEliminado, fechaInicio, inicio, fechaFin, fin);
                        diarioEliminado.EliminarCarpeta(false);
                    }
                    catch (System.Exception ex)
                    {
                        Console.Write(ex.Message);
                    }
                }
                if (Preferencias.cPreferencias.instancia.eliminarPSTsVaciosTrasArchivar)
                    store.QuitarYEliminarSiVacio(false);
            }
            sttContador.Visible = false;
            sttDetener.Visible = false;
            sttProgress.Visible = false;
            CargarPSTs();
            System.Windows.Forms.Application.DoEvents();
            Preferencias.cPreferencias.instancia.detener = false;
            HabilitarAcciones();
        }

        private void sttDetener_ButtonClick(object sender, EventArgs e)
        {
            Preferencias.cPreferencias.instancia.detener = true;
        }

        private void seleccionadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<Store> PSTs = new List<Store>();
            foreach (TreeNode nodo in treePSTs.Nodes)
                if (nodo.Checked && !PSTs.Exists(d => d.StoreID == ((MAPIFolder)nodo.Tag).StoreID))
                    PSTs.Add(((MAPIFolder)nodo.Tag).Store);

            QuitarPSTs(PSTs);
        }

        private void QuitarPSTs(List<Store> lista)
        {
            DeshabilitarAcciones(true);
            int contadorStores = lista.Count - 1;
            while (contadorStores >= 0)
            {
                if (lista[contadorStores].StoreID != ns.DefaultStore.StoreID)
                    lista[contadorStores].QuitarYEliminarSiVacio(true);
                contadorStores--;
            }

            CargarPSTs();
            HabilitarAcciones();
        }

        private void txtNombrePST_TextChanged(object sender, EventArgs e)
        {
            DeshabilitarAcciones(false);
            tmrActualizarPreferencias.Stop();
            tmrActualizarPreferencias.Start();
        }

        private void timerActualizarPreferencias_Tick(object sender, EventArgs e)
        {
            tmrActualizarPreferencias.Stop();

            Preferencias.cPreferencias.instancia.nombrePST = txtNombreArchivo.Text;
            Preferencias.cPreferencias.instancia.Guardar();
            Preferencias.frmPreferencias.Instancia().CargarDatos();

            string expresion = util.TransformarExpresion(txtNombreArchivo.Text);
            if (expresion != "")
            {
                lblNombreArchivo.Text = DateTime.Now.ToString(expresion);
                if (!lblNombreArchivo.Text.EndsWith(".pst"))
                    lblNombreArchivo.Text += ".pst";
                HabilitarAcciones();
            }
            else
            {
                lblNombreArchivo.Text = ".pst";
            }
        }

        internal void ActualizarPreferenciasActuales()
        {
            txtNombreArchivo.Text = Preferencias.cPreferencias.instancia.nombrePST;
        }

        private void frmPrincipal_FormClosed(object sender, FormClosedEventArgs e)
        {
            instancia = null;
        }

        private void actualizarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CargarPSTs();
        }

        private void frmPrincipal_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
                CargarPSTs();
            else if (e.KeyCode == Keys.Escape)
                Close();
        }

        private void todoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            List<KeyValuePair<Store, List<MAPIFolder>>> lista2 = new List<KeyValuePair<Store, List<MAPIFolder>>>();
            List<Store> lista = new List<Store>();

            foreach (TreeNode nodo in treePSTs.Nodes)
                GuardarSubNodos(nodo, false, ref lista2);
            foreach (KeyValuePair<Store, List<MAPIFolder>> par in lista2)
                lista.Add(par.Key);

            EliminarDiarios(ref lista, DateTime.Now, false, DateTime.Now, false);
        }

        private void todoToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            List<KeyValuePair<Store, List<MAPIFolder>>> lista2 = new List<KeyValuePair<Store, List<MAPIFolder>>>();
            List<Store> lista = new List<Store>();

            foreach (TreeNode nodo in treePSTs.Nodes)
                GuardarSubNodos(nodo, true, ref lista2);
            foreach (KeyValuePair<Store, List<MAPIFolder>> par in lista2)
                lista.Add(par.Key);

            EliminarDiarios(ref lista, DateTime.Now, false, DateTime.Now, false);
        }

        private void entreFechasToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            List<KeyValuePair<Store, List<MAPIFolder>>> lista2 = new List<KeyValuePair<Store, List<MAPIFolder>>>();
            List<Store> lista = new List<Store>();

            foreach (TreeNode nodo in treePSTs.Nodes)
                GuardarSubNodos(nodo, false, ref lista2);
            foreach (KeyValuePair<Store, List<MAPIFolder>> par in lista2)
                lista.Add(par.Key);

            frmFechas fechas = new frmFechas();
            DialogResult respuesta = fechas.ShowDialog();
            if (respuesta == DialogResult.OK)
                EliminarDiarios(ref lista, fechas.dateDesde.Value, fechas.dateDesde.Checked, fechas.dateHasta.Value, fechas.dateHasta.Checked);
        }

        private void entreFechasToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            List<KeyValuePair<Store, List<MAPIFolder>>> lista2 = new List<KeyValuePair<Store, List<MAPIFolder>>>();
            List<Store> lista = new List<Store>();

            foreach (TreeNode nodo in treePSTs.Nodes)
                GuardarSubNodos(nodo, true, ref lista2);
            foreach (KeyValuePair<Store, List<MAPIFolder>> par in lista2)
                lista.Add(par.Key);

            frmFechas fechas = new frmFechas();
            DialogResult respuesta = fechas.ShowDialog();
            if (respuesta == DialogResult.OK)
                EliminarDiarios(ref lista, fechas.dateDesde.Value, fechas.dateDesde.Checked, fechas.dateHasta.Value, fechas.dateHasta.Checked);
        }
    }
}
