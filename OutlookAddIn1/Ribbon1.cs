﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Tools.Ribbon;
using System.IO;
using System.Windows.Forms;
using Microsoft.Office.Interop.Outlook;
using OutlookAddIn1.Utiles;
using OutlookAddIn1.Extension;

namespace OutlookAddIn1
{
    public partial class ribonClassify
    {
        cUtil util = new cUtil();

        private void Ribbon1_Load(object sender, RibbonUIEventArgs e)
        {

            Preferencias.cPreferencias.Cargar();
            util.EliminarFicherosInexistentes(ref Preferencias.cPreferencias.instancia.pstsParaEliminar);

            string mensaje = "";
            foreach (string str in Preferencias.cPreferencias.instancia.pstsParaEliminar)
                mensaje += str + "\n";
            if (mensaje != "")
                AbrirEstadoCronoEliminarPSTsPendientes();
        }

        private void ribbonPreferencias_Click(object sender, RibbonControlEventArgs e)
        {
            Preferencias.frmPreferencias preferencias = Preferencias.frmPreferencias.Instancia();
            preferencias.BringToFront();
            preferencias.Show();
        }

        private void btnArchivar_Click(object sender, RibbonControlEventArgs e)
        {
            frmPrincipal principal = frmPrincipal.Instancia();
            principal.BringToFront();
            principal.Show();
        }

        private void EliminarPSTsPendientes()
        {
            tmrEliminarPSTSPendientes.Interval = 60000;
            tmrEliminarPSTSPendientes.Stop();
            int contador = Preferencias.cPreferencias.instancia.pstsParaEliminar.Count - 1;
            while (contador >= 0)
            {
                try
                {
                    if (File.Exists(Preferencias.cPreferencias.instancia.pstsParaEliminar[contador]))
                        File.Delete(Preferencias.cPreferencias.instancia.pstsParaEliminar[contador]);

                    Preferencias.cPreferencias.instancia.pstsParaEliminar.RemoveAt(contador);
                    Preferencias.cPreferencias.instancia.Guardar();
                }
                catch
                {

                }
                contador--;
            }
            if (Preferencias.cPreferencias.instancia.pstsParaEliminar.Count > 0)
                EncenderCronoEliminarPSTs();

            else
                PararCronoEliminarPSTs();
        }

        private void tmrEliminarPSTSPendientes_Tick(object sender, EventArgs e)
        {
            EliminarPSTsPendientes();
        }

        private void button1_Click(object sender, RibbonControlEventArgs e)
        {
            if (fdialAnyadirPSTs.ShowDialog() == DialogResult.OK)
                foreach (string pst in fdialAnyadirPSTs.FileNames)
                    Globals.ThisAddIn.Application.Session.AnyadirPST(pst);
        }

        internal void EncenderCronoEliminarPSTs()
        {
            if (Preferencias.cPreferencias.instancia.eliminarArchivoPSTSiVacio)
            {
                tmrEliminarPSTSPendientes.Start();
                btnEliminarPSTs.Label = "Intentando eliminar PSTs pendientes";
                btnEliminarPSTs.Image = OutlookAddIn1.Properties.Resources.Expirado;
                System.Windows.Forms.Application.DoEvents();
            }
            System.Windows.Forms.Application.DoEvents();
        }

        internal void PararCronoEliminarPSTs()
        {
            tmrEliminarPSTSPendientes.Stop();
            btnEliminarPSTs.Label = "NO intentando eliminar PSTs pendientes";
            btnEliminarPSTs.Image = OutlookAddIn1.Properties.Resources.Hora;
            System.Windows.Forms.Application.DoEvents();
            tmrEliminarPSTSPendientes.Interval = 100;
        }

        private void btnQuitarPSTsVacios_Click(object sender, RibbonControlEventArgs e)
        {
            Globals.ThisAddIn.Application.Session.EliminarPSTsVacios();
        }

        private void AbrirEstadoCronoEliminarPSTsPendientes()
        {
            frmEliminarPSTsVacios eliminarPSTs = new frmEliminarPSTsVacios(ref Preferencias.cPreferencias.instancia.pstsParaEliminar, tmrEliminarPSTSPendientes);
            if (eliminarPSTs.ShowDialog() == DialogResult.OK)
            {
                if (eliminarPSTs.btnEstadoCronometro.Text == "Si")
                    EncenderCronoEliminarPSTs();
                else
                    PararCronoEliminarPSTs();
            }
        }

        private void btnEliminarPSTs_Click(object sender, RibbonControlEventArgs e)
        {
            AbrirEstadoCronoEliminarPSTsPendientes();
        }

        private void btnEliminarCarpetasVacias_Click(object sender, RibbonControlEventArgs e)
        {
            Globals.ThisAddIn.Application.Session.EliminarCarpetasVacias();
        }

        private void btnInfo_Click(object sender, RibbonControlEventArgs e)
        {
            frmInformacion fInfo = new frmInformacion();
            fInfo.Show();
        }
    }
}
