﻿namespace OutlookAddIn1
{
    partial class frmLicencia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabLGPL = new System.Windows.Forms.TabPage();
            this.tabGPL = new System.Windows.Forms.TabPage();
            this.txtGPL = new System.Windows.Forms.RichTextBox();
            this.pctGPL = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pctLGPL = new System.Windows.Forms.PictureBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtLGPL = new System.Windows.Forms.RichTextBox();
            this.tabControl1.SuspendLayout();
            this.tabLGPL.SuspendLayout();
            this.tabGPL.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctGPL)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctLGPL)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabLGPL);
            this.tabControl1.Controls.Add(this.tabGPL);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(607, 449);
            this.tabControl1.TabIndex = 6;
            // 
            // tabLGPL
            // 
            this.tabLGPL.Controls.Add(this.groupBox2);
            this.tabLGPL.Controls.Add(this.pctLGPL);
            this.tabLGPL.Location = new System.Drawing.Point(4, 22);
            this.tabLGPL.Name = "tabLGPL";
            this.tabLGPL.Padding = new System.Windows.Forms.Padding(3);
            this.tabLGPL.Size = new System.Drawing.Size(599, 423);
            this.tabLGPL.TabIndex = 1;
            this.tabLGPL.Text = "LGPL";
            this.tabLGPL.UseVisualStyleBackColor = true;
            // 
            // tabGPL
            // 
            this.tabGPL.Controls.Add(this.groupBox1);
            this.tabGPL.Controls.Add(this.pctGPL);
            this.tabGPL.Location = new System.Drawing.Point(4, 22);
            this.tabGPL.Name = "tabGPL";
            this.tabGPL.Padding = new System.Windows.Forms.Padding(3);
            this.tabGPL.Size = new System.Drawing.Size(599, 423);
            this.tabGPL.TabIndex = 2;
            this.tabGPL.Text = "GPL";
            this.tabGPL.UseVisualStyleBackColor = true;
            // 
            // txtGPL
            // 
            this.txtGPL.BackColor = System.Drawing.SystemColors.Control;
            this.txtGPL.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtGPL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtGPL.Location = new System.Drawing.Point(3, 16);
            this.txtGPL.Name = "txtGPL";
            this.txtGPL.ReadOnly = true;
            this.txtGPL.Size = new System.Drawing.Size(488, 292);
            this.txtGPL.TabIndex = 10;
            this.txtGPL.Text = "";
            // 
            // pctGPL
            // 
            this.pctGPL.BackColor = System.Drawing.Color.Transparent;
            this.pctGPL.BackgroundImage = global::OutlookAddIn1.Properties.Resources.gplv3;
            this.pctGPL.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pctGPL.Location = new System.Drawing.Point(52, 26);
            this.pctGPL.Name = "pctGPL";
            this.pctGPL.Size = new System.Drawing.Size(71, 34);
            this.pctGPL.TabIndex = 9;
            this.pctGPL.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.txtGPL);
            this.groupBox1.Location = new System.Drawing.Point(52, 79);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(494, 311);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "GNU General Public License (GPL)";
            // 
            // pctLGPL
            // 
            this.pctLGPL.BackColor = System.Drawing.Color.Transparent;
            this.pctLGPL.BackgroundImage = global::OutlookAddIn1.Properties.Resources.lgplv3;
            this.pctLGPL.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pctLGPL.Location = new System.Drawing.Point(52, 26);
            this.pctLGPL.Name = "pctLGPL";
            this.pctLGPL.Size = new System.Drawing.Size(71, 34);
            this.pctLGPL.TabIndex = 1;
            this.pctLGPL.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtLGPL);
            this.groupBox2.Location = new System.Drawing.Point(52, 79);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(494, 311);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "GNU Lesser General Public License (LGPL)";
            // 
            // txtLGPL
            // 
            this.txtLGPL.BackColor = System.Drawing.SystemColors.Control;
            this.txtLGPL.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtLGPL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLGPL.Location = new System.Drawing.Point(3, 16);
            this.txtLGPL.Name = "txtLGPL";
            this.txtLGPL.ReadOnly = true;
            this.txtLGPL.Size = new System.Drawing.Size(488, 292);
            this.txtLGPL.TabIndex = 10;
            this.txtLGPL.Text = "";
            // 
            // frmLicencia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(626, 473);
            this.Controls.Add(this.tabControl1);
            this.Name = "frmLicencia";
            this.Text = "Licencia";
            this.Load += new System.EventHandler(this.frmLicencia_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabLGPL.ResumeLayout(false);
            this.tabGPL.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pctGPL)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pctLGPL)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabLGPL;
        private System.Windows.Forms.TabPage tabGPL;
        private System.Windows.Forms.RichTextBox txtGPL;
        private System.Windows.Forms.PictureBox pctGPL;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pctLGPL;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RichTextBox txtLGPL;
    }
}