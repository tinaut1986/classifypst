﻿namespace OutlookAddIn1
{
    partial class frmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.treePSTs = new System.Windows.Forms.TreeView();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.archivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitarPSTsSeleccionadosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.seleccionadosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vaciosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.añadirPSTsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.actualizarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.utilidadesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clasificarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.todasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.todasToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.entreFechasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.seleccionadasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.todoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.entreFechasToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.eliminarCarpetasVaciasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.todasToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.seleccionadasToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.eliminarDiariosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.todosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.todoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.entreFechasToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.seleccionadosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.todoToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.entreFechasToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.sttProgress = new System.Windows.Forms.ToolStripProgressBar();
            this.sttDetener = new System.Windows.Forms.ToolStripSplitButton();
            this.detenerTodoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.detenerActualToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sttContador = new System.Windows.Forms.ToolStripStatusLabel();
            this.tmrCargarArbol = new System.Windows.Forms.Timer(this.components);
            this.tmrActualizarUI = new System.Windows.Forms.Timer(this.components);
            this.fdialAnyadirPSTs = new System.Windows.Forms.OpenFileDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNombreArchivo = new System.Windows.Forms.TextBox();
            this.lblNombreArchivo = new System.Windows.Forms.Label();
            this.tmrActualizarPreferencias = new System.Windows.Forms.Timer(this.components);
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // treePSTs
            // 
            this.treePSTs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.treePSTs.CheckBoxes = true;
            this.treePSTs.Location = new System.Drawing.Point(12, 62);
            this.treePSTs.Name = "treePSTs";
            this.treePSTs.Size = new System.Drawing.Size(561, 542);
            this.treePSTs.TabIndex = 0;
            this.treePSTs.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.treePSTs_AfterCheck);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.archivoToolStripMenuItem,
            this.utilidadesToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(585, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // archivoToolStripMenuItem
            // 
            this.archivoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.quitarPSTsSeleccionadosToolStripMenuItem,
            this.añadirPSTsToolStripMenuItem,
            this.actualizarToolStripMenuItem});
            this.archivoToolStripMenuItem.Name = "archivoToolStripMenuItem";
            this.archivoToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.archivoToolStripMenuItem.Text = "Archivo";
            // 
            // quitarPSTsSeleccionadosToolStripMenuItem
            // 
            this.quitarPSTsSeleccionadosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.seleccionadosToolStripMenuItem,
            this.vaciosToolStripMenuItem});
            this.quitarPSTsSeleccionadosToolStripMenuItem.Image = global::OutlookAddIn1.Properties.Resources.Menos;
            this.quitarPSTsSeleccionadosToolStripMenuItem.Name = "quitarPSTsSeleccionadosToolStripMenuItem";
            this.quitarPSTsSeleccionadosToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.quitarPSTsSeleccionadosToolStripMenuItem.Text = "Quitar PSTs";
            // 
            // seleccionadosToolStripMenuItem
            // 
            this.seleccionadosToolStripMenuItem.Name = "seleccionadosToolStripMenuItem";
            this.seleccionadosToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.seleccionadosToolStripMenuItem.Text = "Seleccionados";
            this.seleccionadosToolStripMenuItem.Click += new System.EventHandler(this.seleccionadosToolStripMenuItem_Click);
            // 
            // vaciosToolStripMenuItem
            // 
            this.vaciosToolStripMenuItem.Name = "vaciosToolStripMenuItem";
            this.vaciosToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.vaciosToolStripMenuItem.Text = "Vacios";
            this.vaciosToolStripMenuItem.Click += new System.EventHandler(this.vaciosToolStripMenuItem_Click);
            // 
            // añadirPSTsToolStripMenuItem
            // 
            this.añadirPSTsToolStripMenuItem.Image = global::OutlookAddIn1.Properties.Resources.Mas;
            this.añadirPSTsToolStripMenuItem.Name = "añadirPSTsToolStripMenuItem";
            this.añadirPSTsToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.añadirPSTsToolStripMenuItem.Text = "Añadir PSTs";
            this.añadirPSTsToolStripMenuItem.Click += new System.EventHandler(this.añadirPSTsToolStripMenuItem_Click);
            // 
            // actualizarToolStripMenuItem
            // 
            this.actualizarToolStripMenuItem.Image = global::OutlookAddIn1.Properties.Resources.Sincronizar;
            this.actualizarToolStripMenuItem.Name = "actualizarToolStripMenuItem";
            this.actualizarToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.actualizarToolStripMenuItem.Text = "Actualizar (F5)";
            this.actualizarToolStripMenuItem.Click += new System.EventHandler(this.actualizarToolStripMenuItem_Click);
            // 
            // utilidadesToolStripMenuItem
            // 
            this.utilidadesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clasificarToolStripMenuItem,
            this.eliminarCarpetasVaciasToolStripMenuItem,
            this.eliminarDiariosToolStripMenuItem});
            this.utilidadesToolStripMenuItem.Name = "utilidadesToolStripMenuItem";
            this.utilidadesToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.utilidadesToolStripMenuItem.Text = "Utilidades";
            // 
            // clasificarToolStripMenuItem
            // 
            this.clasificarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.todasToolStripMenuItem,
            this.seleccionadasToolStripMenuItem});
            this.clasificarToolStripMenuItem.Image = global::OutlookAddIn1.Properties.Resources.Carpeta;
            this.clasificarToolStripMenuItem.Name = "clasificarToolStripMenuItem";
            this.clasificarToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.clasificarToolStripMenuItem.Text = "Archivar carpetas";
            // 
            // todasToolStripMenuItem
            // 
            this.todasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.todasToolStripMenuItem1,
            this.entreFechasToolStripMenuItem});
            this.todasToolStripMenuItem.Name = "todasToolStripMenuItem";
            this.todasToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.todasToolStripMenuItem.Text = "Todas";
            // 
            // todasToolStripMenuItem1
            // 
            this.todasToolStripMenuItem1.Name = "todasToolStripMenuItem1";
            this.todasToolStripMenuItem1.Size = new System.Drawing.Size(138, 22);
            this.todasToolStripMenuItem1.Text = "Todo";
            this.todasToolStripMenuItem1.Click += new System.EventHandler(this.todasToolStripMenuItem1_Click);
            // 
            // entreFechasToolStripMenuItem
            // 
            this.entreFechasToolStripMenuItem.Name = "entreFechasToolStripMenuItem";
            this.entreFechasToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.entreFechasToolStripMenuItem.Text = "Entre fechas";
            this.entreFechasToolStripMenuItem.Click += new System.EventHandler(this.entreFechasToolStripMenuItem_Click);
            // 
            // seleccionadasToolStripMenuItem
            // 
            this.seleccionadasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.todoToolStripMenuItem,
            this.entreFechasToolStripMenuItem1});
            this.seleccionadasToolStripMenuItem.Name = "seleccionadasToolStripMenuItem";
            this.seleccionadasToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.seleccionadasToolStripMenuItem.Text = "Seleccionadas";
            // 
            // todoToolStripMenuItem
            // 
            this.todoToolStripMenuItem.Name = "todoToolStripMenuItem";
            this.todoToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.todoToolStripMenuItem.Text = "Todo";
            this.todoToolStripMenuItem.Click += new System.EventHandler(this.todoToolStripMenuItem_Click);
            // 
            // entreFechasToolStripMenuItem1
            // 
            this.entreFechasToolStripMenuItem1.Name = "entreFechasToolStripMenuItem1";
            this.entreFechasToolStripMenuItem1.Size = new System.Drawing.Size(138, 22);
            this.entreFechasToolStripMenuItem1.Text = "Entre fechas";
            this.entreFechasToolStripMenuItem1.Click += new System.EventHandler(this.entreFechasToolStripMenuItem1_Click);
            // 
            // eliminarCarpetasVaciasToolStripMenuItem
            // 
            this.eliminarCarpetasVaciasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.todasToolStripMenuItem2,
            this.seleccionadasToolStripMenuItem1});
            this.eliminarCarpetasVaciasToolStripMenuItem.Image = global::OutlookAddIn1.Properties.Resources.Papelera_vacia;
            this.eliminarCarpetasVaciasToolStripMenuItem.Name = "eliminarCarpetasVaciasToolStripMenuItem";
            this.eliminarCarpetasVaciasToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.eliminarCarpetasVaciasToolStripMenuItem.Text = "Eliminar carpetas vacias";
            // 
            // todasToolStripMenuItem2
            // 
            this.todasToolStripMenuItem2.Name = "todasToolStripMenuItem2";
            this.todasToolStripMenuItem2.Size = new System.Drawing.Size(152, 22);
            this.todasToolStripMenuItem2.Text = "Todas";
            this.todasToolStripMenuItem2.Click += new System.EventHandler(this.todasToolStripMenuItem2_Click);
            // 
            // seleccionadasToolStripMenuItem1
            // 
            this.seleccionadasToolStripMenuItem1.Name = "seleccionadasToolStripMenuItem1";
            this.seleccionadasToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.seleccionadasToolStripMenuItem1.Text = "Seleccionadas";
            this.seleccionadasToolStripMenuItem1.Click += new System.EventHandler(this.seleccionadasToolStripMenuItem1_Click);
            // 
            // eliminarDiariosToolStripMenuItem
            // 
            this.eliminarDiariosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.todosToolStripMenuItem,
            this.seleccionadosToolStripMenuItem1});
            this.eliminarDiariosToolStripMenuItem.Image = global::OutlookAddIn1.Properties.Resources.Eliminar_fila;
            this.eliminarDiariosToolStripMenuItem.Name = "eliminarDiariosToolStripMenuItem";
            this.eliminarDiariosToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.eliminarDiariosToolStripMenuItem.Text = "Eliminar diarios";
            // 
            // todosToolStripMenuItem
            // 
            this.todosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.todoToolStripMenuItem1,
            this.entreFechasToolStripMenuItem2});
            this.todosToolStripMenuItem.Name = "todosToolStripMenuItem";
            this.todosToolStripMenuItem.Size = new System.Drawing.Size(237, 22);
            this.todosToolStripMenuItem.Text = "Todos los PSTs";
            // 
            // todoToolStripMenuItem1
            // 
            this.todoToolStripMenuItem1.Name = "todoToolStripMenuItem1";
            this.todoToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.todoToolStripMenuItem1.Text = "Todo";
            this.todoToolStripMenuItem1.Click += new System.EventHandler(this.todoToolStripMenuItem1_Click);
            // 
            // entreFechasToolStripMenuItem2
            // 
            this.entreFechasToolStripMenuItem2.Name = "entreFechasToolStripMenuItem2";
            this.entreFechasToolStripMenuItem2.Size = new System.Drawing.Size(152, 22);
            this.entreFechasToolStripMenuItem2.Text = "Entre fechas";
            this.entreFechasToolStripMenuItem2.Click += new System.EventHandler(this.entreFechasToolStripMenuItem2_Click);
            // 
            // seleccionadosToolStripMenuItem1
            // 
            this.seleccionadosToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.todoToolStripMenuItem2,
            this.entreFechasToolStripMenuItem3});
            this.seleccionadosToolStripMenuItem1.Name = "seleccionadosToolStripMenuItem1";
            this.seleccionadosToolStripMenuItem1.Size = new System.Drawing.Size(237, 22);
            this.seleccionadosToolStripMenuItem1.Text = "PSTs de carpetas seleccionadas";
            // 
            // todoToolStripMenuItem2
            // 
            this.todoToolStripMenuItem2.Name = "todoToolStripMenuItem2";
            this.todoToolStripMenuItem2.Size = new System.Drawing.Size(152, 22);
            this.todoToolStripMenuItem2.Text = "Todo";
            this.todoToolStripMenuItem2.Click += new System.EventHandler(this.todoToolStripMenuItem2_Click);
            // 
            // entreFechasToolStripMenuItem3
            // 
            this.entreFechasToolStripMenuItem3.Name = "entreFechasToolStripMenuItem3";
            this.entreFechasToolStripMenuItem3.Size = new System.Drawing.Size(152, 22);
            this.entreFechasToolStripMenuItem3.Text = "Entre fechas";
            this.entreFechasToolStripMenuItem3.Click += new System.EventHandler(this.entreFechasToolStripMenuItem3_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sttProgress,
            this.sttDetener,
            this.sttContador});
            this.statusStrip1.Location = new System.Drawing.Point(0, 607);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(585, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // sttProgress
            // 
            this.sttProgress.Name = "sttProgress";
            this.sttProgress.Size = new System.Drawing.Size(100, 16);
            this.sttProgress.Visible = false;
            // 
            // sttDetener
            // 
            this.sttDetener.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.sttDetener.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.detenerTodoToolStripMenuItem,
            this.detenerActualToolStripMenuItem});
            this.sttDetener.Image = global::OutlookAddIn1.Properties.Resources.Cancelar;
            this.sttDetener.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.sttDetener.Name = "sttDetener";
            this.sttDetener.Size = new System.Drawing.Size(32, 20);
            this.sttDetener.Text = "toolStripSplitButton1";
            this.sttDetener.Visible = false;
            this.sttDetener.ButtonClick += new System.EventHandler(this.sttDetener_ButtonClick);
            // 
            // detenerTodoToolStripMenuItem
            // 
            this.detenerTodoToolStripMenuItem.Name = "detenerTodoToolStripMenuItem";
            this.detenerTodoToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.detenerTodoToolStripMenuItem.Text = "Detener todo";
            // 
            // detenerActualToolStripMenuItem
            // 
            this.detenerActualToolStripMenuItem.Name = "detenerActualToolStripMenuItem";
            this.detenerActualToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.detenerActualToolStripMenuItem.Text = "Detener actual";
            // 
            // sttContador
            // 
            this.sttContador.Name = "sttContador";
            this.sttContador.Size = new System.Drawing.Size(24, 17);
            this.sttContador.Text = "0/0";
            this.sttContador.Visible = false;
            // 
            // tmrCargarArbol
            // 
            this.tmrCargarArbol.Tick += new System.EventHandler(this.tmrCargarArbol_Tick);
            // 
            // tmrActualizarUI
            // 
            this.tmrActualizarUI.Interval = 500;
            this.tmrActualizarUI.Tick += new System.EventHandler(this.tmrActualizarUI_Tick);
            // 
            // fdialAnyadirPSTs
            // 
            this.fdialAnyadirPSTs.FileName = "Archivo.pst";
            this.fdialAnyadirPSTs.Filter = "Almacenes de Outlook|*.pst|Todos los archiovs|*.*";
            this.fdialAnyadirPSTs.Multiselect = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(50, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Nombre de PST";
            // 
            // txtNombreArchivo
            // 
            this.txtNombreArchivo.Location = new System.Drawing.Point(139, 36);
            this.txtNombreArchivo.Name = "txtNombreArchivo";
            this.txtNombreArchivo.Size = new System.Drawing.Size(219, 20);
            this.txtNombreArchivo.TabIndex = 4;
            this.txtNombreArchivo.TextChanged += new System.EventHandler(this.txtNombrePST_TextChanged);
            // 
            // lblNombreArchivo
            // 
            this.lblNombreArchivo.AutoSize = true;
            this.lblNombreArchivo.Location = new System.Drawing.Point(364, 40);
            this.lblNombreArchivo.Name = "lblNombreArchivo";
            this.lblNombreArchivo.Size = new System.Drawing.Size(109, 13);
            this.lblNombreArchivo.TabIndex = 3;
            this.lblNombreArchivo.Text = "Archivo - <YYYY>.pst";
            // 
            // tmrActualizarPreferencias
            // 
            this.tmrActualizarPreferencias.Interval = 500;
            this.tmrActualizarPreferencias.Tick += new System.EventHandler(this.timerActualizarPreferencias_Tick);
            // 
            // frmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(585, 629);
            this.Controls.Add(this.txtNombreArchivo);
            this.Controls.Add(this.lblNombreArchivo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.treePSTs);
            this.Controls.Add(this.menuStrip1);
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(601, 668);
            this.Name = "frmPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Archivar elementos";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmPrincipal_FormClosed);
            this.Load += new System.EventHandler(this.frmPrincipal_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmPrincipal_KeyDown);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView treePSTs;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Timer tmrCargarArbol;
        private System.Windows.Forms.ToolStripMenuItem archivoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitarPSTsSeleccionadosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem seleccionadosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vaciosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem utilidadesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clasificarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem todasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem todasToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem entreFechasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem seleccionadasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eliminarCarpetasVaciasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem todoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem entreFechasToolStripMenuItem1;
        private System.Windows.Forms.ToolStripProgressBar sttProgress;
        private System.Windows.Forms.ToolStripSplitButton sttDetener;
        private System.Windows.Forms.ToolStripMenuItem detenerActualToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem detenerTodoToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel sttContador;
        private System.Windows.Forms.Timer tmrActualizarUI;
        private System.Windows.Forms.ToolStripMenuItem añadirPSTsToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog fdialAnyadirPSTs;
        private System.Windows.Forms.ToolStripMenuItem todasToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem seleccionadasToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem eliminarDiariosToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNombreArchivo;
        private System.Windows.Forms.Label lblNombreArchivo;
        private System.Windows.Forms.Timer tmrActualizarPreferencias;
        private System.Windows.Forms.ToolStripMenuItem actualizarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem todosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem todoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem entreFechasToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem seleccionadosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem todoToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem entreFechasToolStripMenuItem3;
    }
}