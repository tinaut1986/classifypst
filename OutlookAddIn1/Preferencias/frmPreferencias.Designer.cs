﻿namespace OutlookAddIn1.Preferencias
{
    partial class frmPreferencias
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPreferencias));
            this.chkEliminarDirectamente = new System.Windows.Forms.CheckBox();
            this.btnAplicar = new System.Windows.Forms.Button();
            this.txtRutaPST = new System.Windows.Forms.TextBox();
            this.labelRutaPST = new System.Windows.Forms.Label();
            this.txtNombreArchivo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtExtension = new System.Windows.Forms.TextBox();
            this.btnAyuda = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.lblResultado = new System.Windows.Forms.Label();
            this.chkRenombrarRaiz = new System.Windows.Forms.CheckBox();
            this.chkEliminarPSTsVaciosTrasArchiar = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkEliminarCarpetasVaciasDespuesDeArchivar = new System.Windows.Forms.CheckBox();
            this.chkEliminarArchivoPSTSiVacio = new System.Windows.Forms.CheckBox();
            this.chkArchivarElementosSegunImportancia = new System.Windows.Forms.CheckBox();
            this.grpArchivarSegunImportancia = new System.Windows.Forms.GroupBox();
            this.lblImportanciaNota = new System.Windows.Forms.Label();
            this.chkArchivarElementosNoImportantes = new System.Windows.Forms.CheckBox();
            this.chkArchivarElementosImportantes = new System.Windows.Forms.CheckBox();
            this.chkArchivarElementosNormales = new System.Windows.Forms.CheckBox();
            this.chkArchivarElementosNoLeidos = new System.Windows.Forms.CheckBox();
            this.tabPreferencias = new System.Windows.Forms.TabControl();
            this.tabGenerales = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tabFechas1 = new System.Windows.Forms.TabPage();
            this.btnDefectoFechas = new System.Windows.Forms.Button();
            this.chkTarea = new System.Windows.Forms.CheckBox();
            this.chkNota = new System.Windows.Forms.CheckBox();
            this.chkContacto = new System.Windows.Forms.CheckBox();
            this.chkCita = new System.Windows.Forms.CheckBox();
            this.chkReunion = new System.Windows.Forms.CheckBox();
            this.chkCorreoPublico = new System.Windows.Forms.CheckBox();
            this.chkCorreo = new System.Windows.Forms.CheckBox();
            this.grpTarea = new System.Windows.Forms.GroupBox();
            this.radTareaRecordatorio = new System.Windows.Forms.RadioButton();
            this.radTareaModificacion = new System.Windows.Forms.RadioButton();
            this.radTareaCreacion = new System.Windows.Forms.RadioButton();
            this.grpNota = new System.Windows.Forms.GroupBox();
            this.radNotaModificacion = new System.Windows.Forms.RadioButton();
            this.radNotaCreacion = new System.Windows.Forms.RadioButton();
            this.grpContacto = new System.Windows.Forms.GroupBox();
            this.radContactoRecordatorio = new System.Windows.Forms.RadioButton();
            this.radContactoModificacion = new System.Windows.Forms.RadioButton();
            this.radContactoCreacion = new System.Windows.Forms.RadioButton();
            this.grpCita = new System.Windows.Forms.GroupBox();
            this.radCitaRespuesta = new System.Windows.Forms.RadioButton();
            this.radCitaModificacion = new System.Windows.Forms.RadioButton();
            this.radCitaFin = new System.Windows.Forms.RadioButton();
            this.radCitaInicio = new System.Windows.Forms.RadioButton();
            this.radCitaCreacion = new System.Windows.Forms.RadioButton();
            this.grpReunion = new System.Windows.Forms.GroupBox();
            this.radReunionRecordatorio = new System.Windows.Forms.RadioButton();
            this.radReunionRecepcion = new System.Windows.Forms.RadioButton();
            this.radReunionModificacion = new System.Windows.Forms.RadioButton();
            this.radReunionCaducidad = new System.Windows.Forms.RadioButton();
            this.radReunionEntregaDiferida = new System.Windows.Forms.RadioButton();
            this.radReunionCreacion = new System.Windows.Forms.RadioButton();
            this.grpCorreoPublico = new System.Windows.Forms.GroupBox();
            this.radCorreoPublicoRecordatorio = new System.Windows.Forms.RadioButton();
            this.radCorreoPublicoRecepcion = new System.Windows.Forms.RadioButton();
            this.radCorreoPublicoModificacion = new System.Windows.Forms.RadioButton();
            this.radCorreoPublicoCaducidad = new System.Windows.Forms.RadioButton();
            this.radCorreoPublicoCreacion = new System.Windows.Forms.RadioButton();
            this.grpCorreo = new System.Windows.Forms.GroupBox();
            this.radCorreoRecordatorio = new System.Windows.Forms.RadioButton();
            this.radCorreoRecepcion = new System.Windows.Forms.RadioButton();
            this.radCorreoModificacion = new System.Windows.Forms.RadioButton();
            this.radCorreoCaducidad = new System.Windows.Forms.RadioButton();
            this.radCorreoEtnregaDiferida = new System.Windows.Forms.RadioButton();
            this.radCorreoCreacion = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.tabFechas2 = new System.Windows.Forms.TabPage();
            this.grpIntercambio = new System.Windows.Forms.GroupBox();
            this.radIntercambioRecordatorio = new System.Windows.Forms.RadioButton();
            this.radIntercambioRecepcion = new System.Windows.Forms.RadioButton();
            this.radIntercambioModificacion = new System.Windows.Forms.RadioButton();
            this.radIntercambioCaducidad = new System.Windows.Forms.RadioButton();
            this.radIntercambioEntregaDiferida = new System.Windows.Forms.RadioButton();
            this.radIntercambioCreacion = new System.Windows.Forms.RadioButton();
            this.chkIntercambio = new System.Windows.Forms.CheckBox();
            this.chkSolicitudAct = new System.Windows.Forms.CheckBox();
            this.chkSolicitudRech = new System.Windows.Forms.CheckBox();
            this.chkSolicitudAcept = new System.Windows.Forms.CheckBox();
            this.chkSolicitud = new System.Windows.Forms.CheckBox();
            this.grpSolicitudAct = new System.Windows.Forms.GroupBox();
            this.radSolicitudActModificacion = new System.Windows.Forms.RadioButton();
            this.radSolicitudActCreacion = new System.Windows.Forms.RadioButton();
            this.grpSolicitudRech = new System.Windows.Forms.GroupBox();
            this.radSolicitudRechModificacion = new System.Windows.Forms.RadioButton();
            this.radSolicitudRechCreacion = new System.Windows.Forms.RadioButton();
            this.grpSolicitudAcept = new System.Windows.Forms.GroupBox();
            this.radSolicitudAceptModificacion = new System.Windows.Forms.RadioButton();
            this.radSolicitudAceptCreacion = new System.Windows.Forms.RadioButton();
            this.grpSolicitud = new System.Windows.Forms.GroupBox();
            this.radSolicitudModificacion = new System.Windows.Forms.RadioButton();
            this.radSolicitudCreacion = new System.Windows.Forms.RadioButton();
            this.chkDiario = new System.Windows.Forms.CheckBox();
            this.btnDefectoFechas2 = new System.Windows.Forms.Button();
            this.grpDiario = new System.Windows.Forms.GroupBox();
            this.radDiarioModificacion = new System.Windows.Forms.RadioButton();
            this.radDiarioCreacion = new System.Windows.Forms.RadioButton();
            this.chkInforme = new System.Windows.Forms.CheckBox();
            this.grpInforme = new System.Windows.Forms.GroupBox();
            this.radInformeModificacion = new System.Windows.Forms.RadioButton();
            this.radInformeCreacion = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.grpArchivarSegunImportancia.SuspendLayout();
            this.tabPreferencias.SuspendLayout();
            this.tabGenerales.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabFechas1.SuspendLayout();
            this.grpTarea.SuspendLayout();
            this.grpNota.SuspendLayout();
            this.grpContacto.SuspendLayout();
            this.grpCita.SuspendLayout();
            this.grpReunion.SuspendLayout();
            this.grpCorreoPublico.SuspendLayout();
            this.grpCorreo.SuspendLayout();
            this.tabFechas2.SuspendLayout();
            this.grpIntercambio.SuspendLayout();
            this.grpSolicitudAct.SuspendLayout();
            this.grpSolicitudRech.SuspendLayout();
            this.grpSolicitudAcept.SuspendLayout();
            this.grpSolicitud.SuspendLayout();
            this.grpDiario.SuspendLayout();
            this.grpInforme.SuspendLayout();
            this.SuspendLayout();
            // 
            // chkEliminarDirectamente
            // 
            this.chkEliminarDirectamente.AutoSize = true;
            this.chkEliminarDirectamente.Location = new System.Drawing.Point(125, 28);
            this.chkEliminarDirectamente.Name = "chkEliminarDirectamente";
            this.chkEliminarDirectamente.Size = new System.Drawing.Size(381, 17);
            this.chkEliminarDirectamente.TabIndex = 0;
            this.chkEliminarDirectamente.Text = "Eliminar los elementos directamente (Sin pasar por \"Elementos eliminados\").";
            this.chkEliminarDirectamente.UseVisualStyleBackColor = true;
            // 
            // btnAplicar
            // 
            this.btnAplicar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAplicar.Location = new System.Drawing.Point(717, 645);
            this.btnAplicar.Name = "btnAplicar";
            this.btnAplicar.Size = new System.Drawing.Size(75, 23);
            this.btnAplicar.TabIndex = 1;
            this.btnAplicar.Text = "Aplicar";
            this.btnAplicar.UseVisualStyleBackColor = true;
            this.btnAplicar.Click += new System.EventHandler(this.btnAplicar_Click);
            // 
            // txtRutaPST
            // 
            this.txtRutaPST.Location = new System.Drawing.Point(138, 19);
            this.txtRutaPST.Name = "txtRutaPST";
            this.txtRutaPST.Size = new System.Drawing.Size(374, 20);
            this.txtRutaPST.TabIndex = 2;
            this.txtRutaPST.Text = "C:\\Temp\\";
            // 
            // labelRutaPST
            // 
            this.labelRutaPST.AutoSize = true;
            this.labelRutaPST.Location = new System.Drawing.Point(10, 22);
            this.labelRutaPST.Name = "labelRutaPST";
            this.labelRutaPST.Size = new System.Drawing.Size(122, 13);
            this.labelRutaPST.TabIndex = 3;
            this.labelRutaPST.Text = "Ruta destino de los PST";
            // 
            // txtNombreArchivo
            // 
            this.txtNombreArchivo.Location = new System.Drawing.Point(138, 45);
            this.txtNombreArchivo.Name = "txtNombreArchivo";
            this.txtNombreArchivo.Size = new System.Drawing.Size(344, 20);
            this.txtNombreArchivo.TabIndex = 2;
            this.txtNombreArchivo.Text = "Archivo - <yyyy>";
            this.txtNombreArchivo.TextChanged += new System.EventHandler(this.txtNombreArchivo_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Nombre de los PST";
            // 
            // txtExtension
            // 
            this.txtExtension.Enabled = false;
            this.txtExtension.Location = new System.Drawing.Point(488, 45);
            this.txtExtension.Name = "txtExtension";
            this.txtExtension.Size = new System.Drawing.Size(24, 20);
            this.txtExtension.TabIndex = 2;
            this.txtExtension.Text = ".pst";
            // 
            // btnAyuda
            // 
            this.btnAyuda.Location = new System.Drawing.Point(519, 45);
            this.btnAyuda.Name = "btnAyuda";
            this.btnAyuda.Size = new System.Drawing.Size(26, 20);
            this.btnAyuda.TabIndex = 4;
            this.btnAyuda.Text = "?";
            this.toolTip1.SetToolTip(this.btnAyuda, resources.GetString("btnAyuda.ToolTip"));
            this.btnAyuda.UseVisualStyleBackColor = true;
            this.btnAyuda.Click += new System.EventHandler(this.btnAyuda_Click);
            // 
            // lblResultado
            // 
            this.lblResultado.AutoSize = true;
            this.lblResultado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblResultado.Location = new System.Drawing.Point(138, 72);
            this.lblResultado.Name = "lblResultado";
            this.lblResultado.Size = new System.Drawing.Size(103, 15);
            this.lblResultado.TabIndex = 5;
            this.lblResultado.Text = "Archivo - <yyyy>.pst";
            // 
            // chkRenombrarRaiz
            // 
            this.chkRenombrarRaiz.AutoSize = true;
            this.chkRenombrarRaiz.Location = new System.Drawing.Point(125, 122);
            this.chkRenombrarRaiz.Name = "chkRenombrarRaiz";
            this.chkRenombrarRaiz.Size = new System.Drawing.Size(315, 17);
            this.chkRenombrarRaiz.TabIndex = 0;
            this.chkRenombrarRaiz.Text = "Renombrar la raiz de los PSTs existentes al mover los correos";
            this.chkRenombrarRaiz.UseVisualStyleBackColor = true;
            // 
            // chkEliminarPSTsVaciosTrasArchiar
            // 
            this.chkEliminarPSTsVaciosTrasArchiar.AutoSize = true;
            this.chkEliminarPSTsVaciosTrasArchiar.Location = new System.Drawing.Point(125, 76);
            this.chkEliminarPSTsVaciosTrasArchiar.Name = "chkEliminarPSTsVaciosTrasArchiar";
            this.chkEliminarPSTsVaciosTrasArchiar.Size = new System.Drawing.Size(355, 17);
            this.chkEliminarPSTsVaciosTrasArchiar.TabIndex = 0;
            this.chkEliminarPSTsVaciosTrasArchiar.Text = "Eliminar directamente PSTs vacios después de archivar los elementos";
            this.chkEliminarPSTsVaciosTrasArchiar.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.txtRutaPST);
            this.groupBox1.Controls.Add(this.txtNombreArchivo);
            this.groupBox1.Controls.Add(this.txtExtension);
            this.groupBox1.Controls.Add(this.labelRutaPST);
            this.groupBox1.Controls.Add(this.lblResultado);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnAyuda);
            this.groupBox1.Location = new System.Drawing.Point(33, 18);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(557, 102);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Nombre y ruta del PST";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.chkEliminarCarpetasVaciasDespuesDeArchivar);
            this.groupBox2.Controls.Add(this.chkEliminarDirectamente);
            this.groupBox2.Controls.Add(this.chkEliminarArchivoPSTSiVacio);
            this.groupBox2.Controls.Add(this.chkEliminarPSTsVaciosTrasArchiar);
            this.groupBox2.Controls.Add(this.chkRenombrarRaiz);
            this.groupBox2.Location = new System.Drawing.Point(33, 126);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(557, 172);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Opciones durante el archivado";
            // 
            // chkEliminarCarpetasVaciasDespuesDeArchivar
            // 
            this.chkEliminarCarpetasVaciasDespuesDeArchivar.AutoSize = true;
            this.chkEliminarCarpetasVaciasDespuesDeArchivar.Location = new System.Drawing.Point(125, 51);
            this.chkEliminarCarpetasVaciasDespuesDeArchivar.Name = "chkEliminarCarpetasVaciasDespuesDeArchivar";
            this.chkEliminarCarpetasVaciasDespuesDeArchivar.Size = new System.Drawing.Size(268, 17);
            this.chkEliminarCarpetasVaciasDespuesDeArchivar.TabIndex = 0;
            this.chkEliminarCarpetasVaciasDespuesDeArchivar.Text = "Eliminar las carpetas vacias después de archivarlas";
            this.chkEliminarCarpetasVaciasDespuesDeArchivar.UseVisualStyleBackColor = true;
            // 
            // chkEliminarArchivoPSTSiVacio
            // 
            this.chkEliminarArchivoPSTSiVacio.AutoSize = true;
            this.chkEliminarArchivoPSTSiVacio.Location = new System.Drawing.Point(124, 99);
            this.chkEliminarArchivoPSTSiVacio.Name = "chkEliminarArchivoPSTSiVacio";
            this.chkEliminarArchivoPSTSiVacio.Size = new System.Drawing.Size(392, 17);
            this.chkEliminarArchivoPSTSiVacio.TabIndex = 0;
            this.chkEliminarArchivoPSTSiVacio.Text = "Intentar eliminar el archivo PST si está vacio después de quitarlo de la cuenta";
            this.chkEliminarArchivoPSTSiVacio.UseVisualStyleBackColor = true;
            // 
            // chkArchivarElementosSegunImportancia
            // 
            this.chkArchivarElementosSegunImportancia.AutoSize = true;
            this.chkArchivarElementosSegunImportancia.Location = new System.Drawing.Point(125, 42);
            this.chkArchivarElementosSegunImportancia.Name = "chkArchivarElementosSegunImportancia";
            this.chkArchivarElementosSegunImportancia.Size = new System.Drawing.Size(263, 17);
            this.chkArchivarElementosSegunImportancia.TabIndex = 9;
            this.chkArchivarElementosSegunImportancia.Text = "Archivar elementos según la importancia marcada.";
            this.chkArchivarElementosSegunImportancia.UseVisualStyleBackColor = true;
            this.chkArchivarElementosSegunImportancia.CheckedChanged += new System.EventHandler(this.chkArchivarElementosSegunImportancia_CheckedChanged);
            // 
            // grpArchivarSegunImportancia
            // 
            this.grpArchivarSegunImportancia.Controls.Add(this.lblImportanciaNota);
            this.grpArchivarSegunImportancia.Controls.Add(this.chkArchivarElementosNoImportantes);
            this.grpArchivarSegunImportancia.Controls.Add(this.chkArchivarElementosImportantes);
            this.grpArchivarSegunImportancia.Controls.Add(this.chkArchivarElementosNormales);
            this.grpArchivarSegunImportancia.Enabled = false;
            this.grpArchivarSegunImportancia.Location = new System.Drawing.Point(138, 65);
            this.grpArchivarSegunImportancia.Name = "grpArchivarSegunImportancia";
            this.grpArchivarSegunImportancia.Size = new System.Drawing.Size(395, 79);
            this.grpArchivarSegunImportancia.TabIndex = 8;
            this.grpArchivarSegunImportancia.TabStop = false;
            this.grpArchivarSegunImportancia.Text = "Importancia";
            // 
            // lblImportanciaNota
            // 
            this.lblImportanciaNota.ForeColor = System.Drawing.Color.DarkRed;
            this.lblImportanciaNota.Location = new System.Drawing.Point(23, 16);
            this.lblImportanciaNota.Name = "lblImportanciaNota";
            this.lblImportanciaNota.Size = new System.Drawing.Size(348, 31);
            this.lblImportanciaNota.TabIndex = 1;
            this.lblImportanciaNota.Text = "*Las notas no tienen importancia, así que sólo se archivarán según el Filtro de F" +
    "echas*";
            // 
            // chkArchivarElementosNoImportantes
            // 
            this.chkArchivarElementosNoImportantes.AutoSize = true;
            this.chkArchivarElementosNoImportantes.Location = new System.Drawing.Point(218, 50);
            this.chkArchivarElementosNoImportantes.Name = "chkArchivarElementosNoImportantes";
            this.chkArchivarElementosNoImportantes.Size = new System.Drawing.Size(103, 17);
            this.chkArchivarElementosNoImportantes.TabIndex = 0;
            this.chkArchivarElementosNoImportantes.Tag = "0";
            this.chkArchivarElementosNoImportantes.Text = "Poco importante";
            this.chkArchivarElementosNoImportantes.UseVisualStyleBackColor = true;
            // 
            // chkArchivarElementosImportantes
            // 
            this.chkArchivarElementosImportantes.AutoSize = true;
            this.chkArchivarElementosImportantes.Location = new System.Drawing.Point(71, 50);
            this.chkArchivarElementosImportantes.Name = "chkArchivarElementosImportantes";
            this.chkArchivarElementosImportantes.Size = new System.Drawing.Size(76, 17);
            this.chkArchivarElementosImportantes.TabIndex = 0;
            this.chkArchivarElementosImportantes.Tag = "2";
            this.chkArchivarElementosImportantes.Text = "Importante";
            this.chkArchivarElementosImportantes.UseVisualStyleBackColor = true;
            // 
            // chkArchivarElementosNormales
            // 
            this.chkArchivarElementosNormales.AutoSize = true;
            this.chkArchivarElementosNormales.Location = new System.Drawing.Point(153, 50);
            this.chkArchivarElementosNormales.Name = "chkArchivarElementosNormales";
            this.chkArchivarElementosNormales.Size = new System.Drawing.Size(59, 17);
            this.chkArchivarElementosNormales.TabIndex = 0;
            this.chkArchivarElementosNormales.Tag = "1";
            this.chkArchivarElementosNormales.Text = "Normal";
            this.chkArchivarElementosNormales.UseVisualStyleBackColor = true;
            // 
            // chkArchivarElementosNoLeidos
            // 
            this.chkArchivarElementosNoLeidos.AutoSize = true;
            this.chkArchivarElementosNoLeidos.Location = new System.Drawing.Point(125, 19);
            this.chkArchivarElementosNoLeidos.Name = "chkArchivarElementosNoLeidos";
            this.chkArchivarElementosNoLeidos.Size = new System.Drawing.Size(217, 17);
            this.chkArchivarElementosNoLeidos.TabIndex = 0;
            this.chkArchivarElementosNoLeidos.Text = "Archivar también los elementos no leidos";
            this.chkArchivarElementosNoLeidos.UseVisualStyleBackColor = true;
            // 
            // tabPreferencias
            // 
            this.tabPreferencias.Controls.Add(this.tabGenerales);
            this.tabPreferencias.Controls.Add(this.tabFechas1);
            this.tabPreferencias.Controls.Add(this.tabFechas2);
            this.tabPreferencias.Location = new System.Drawing.Point(161, 12);
            this.tabPreferencias.Name = "tabPreferencias";
            this.tabPreferencias.SelectedIndex = 0;
            this.tabPreferencias.Size = new System.Drawing.Size(631, 627);
            this.tabPreferencias.TabIndex = 8;
            // 
            // tabGenerales
            // 
            this.tabGenerales.Controls.Add(this.button1);
            this.tabGenerales.Controls.Add(this.groupBox3);
            this.tabGenerales.Controls.Add(this.groupBox2);
            this.tabGenerales.Controls.Add(this.groupBox1);
            this.tabGenerales.Location = new System.Drawing.Point(4, 22);
            this.tabGenerales.Name = "tabGenerales";
            this.tabGenerales.Padding = new System.Windows.Forms.Padding(3);
            this.tabGenerales.Size = new System.Drawing.Size(623, 601);
            this.tabGenerales.TabIndex = 0;
            this.tabGenerales.Text = "Generales";
            this.tabGenerales.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(28, 564);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 9;
            this.button1.Text = "Por defecto";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.btnDefectoGenerales_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.chkArchivarElementosSegunImportancia);
            this.groupBox3.Controls.Add(this.chkArchivarElementosNoLeidos);
            this.groupBox3.Controls.Add(this.grpArchivarSegunImportancia);
            this.groupBox3.Location = new System.Drawing.Point(33, 304);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(557, 154);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Filtro para archivar";
            // 
            // tabFechas1
            // 
            this.tabFechas1.Controls.Add(this.btnDefectoFechas);
            this.tabFechas1.Controls.Add(this.chkTarea);
            this.tabFechas1.Controls.Add(this.chkNota);
            this.tabFechas1.Controls.Add(this.chkContacto);
            this.tabFechas1.Controls.Add(this.chkCita);
            this.tabFechas1.Controls.Add(this.chkReunion);
            this.tabFechas1.Controls.Add(this.chkCorreoPublico);
            this.tabFechas1.Controls.Add(this.chkCorreo);
            this.tabFechas1.Controls.Add(this.grpTarea);
            this.tabFechas1.Controls.Add(this.grpNota);
            this.tabFechas1.Controls.Add(this.grpContacto);
            this.tabFechas1.Controls.Add(this.grpCita);
            this.tabFechas1.Controls.Add(this.grpReunion);
            this.tabFechas1.Controls.Add(this.grpCorreoPublico);
            this.tabFechas1.Controls.Add(this.grpCorreo);
            this.tabFechas1.Controls.Add(this.label2);
            this.tabFechas1.Location = new System.Drawing.Point(4, 22);
            this.tabFechas1.Name = "tabFechas1";
            this.tabFechas1.Padding = new System.Windows.Forms.Padding(3);
            this.tabFechas1.Size = new System.Drawing.Size(623, 601);
            this.tabFechas1.TabIndex = 1;
            this.tabFechas1.Text = "Filtro de fechas 1";
            this.tabFechas1.UseVisualStyleBackColor = true;
            // 
            // btnDefectoFechas
            // 
            this.btnDefectoFechas.Location = new System.Drawing.Point(28, 564);
            this.btnDefectoFechas.Name = "btnDefectoFechas";
            this.btnDefectoFechas.Size = new System.Drawing.Size(75, 23);
            this.btnDefectoFechas.TabIndex = 3;
            this.btnDefectoFechas.Text = "Por defecto";
            this.btnDefectoFechas.UseVisualStyleBackColor = true;
            this.btnDefectoFechas.Click += new System.EventHandler(this.btnDefectoFechas_Click);
            // 
            // chkTarea
            // 
            this.chkTarea.AutoSize = true;
            this.chkTarea.Checked = true;
            this.chkTarea.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTarea.Location = new System.Drawing.Point(62, 512);
            this.chkTarea.Name = "chkTarea";
            this.chkTarea.Size = new System.Drawing.Size(15, 14);
            this.chkTarea.TabIndex = 2;
            this.chkTarea.UseVisualStyleBackColor = true;
            this.chkTarea.CheckedChanged += new System.EventHandler(this.chkTarea_CheckedChanged);
            // 
            // chkNota
            // 
            this.chkNota.AutoSize = true;
            this.chkNota.Location = new System.Drawing.Point(62, 451);
            this.chkNota.Name = "chkNota";
            this.chkNota.Size = new System.Drawing.Size(15, 14);
            this.chkNota.TabIndex = 2;
            this.chkNota.UseVisualStyleBackColor = true;
            this.chkNota.CheckedChanged += new System.EventHandler(this.chkNota_CheckedChanged);
            // 
            // chkContacto
            // 
            this.chkContacto.AutoSize = true;
            this.chkContacto.Location = new System.Drawing.Point(62, 305);
            this.chkContacto.Name = "chkContacto";
            this.chkContacto.Size = new System.Drawing.Size(15, 14);
            this.chkContacto.TabIndex = 2;
            this.chkContacto.UseVisualStyleBackColor = true;
            this.chkContacto.CheckedChanged += new System.EventHandler(this.chkContacto_CheckedChanged);
            // 
            // chkCita
            // 
            this.chkCita.AutoSize = true;
            this.chkCita.Checked = true;
            this.chkCita.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkCita.Location = new System.Drawing.Point(62, 232);
            this.chkCita.Name = "chkCita";
            this.chkCita.Size = new System.Drawing.Size(15, 14);
            this.chkCita.TabIndex = 2;
            this.chkCita.UseVisualStyleBackColor = true;
            this.chkCita.CheckedChanged += new System.EventHandler(this.chkCita_CheckedChanged);
            // 
            // chkReunion
            // 
            this.chkReunion.AutoSize = true;
            this.chkReunion.Checked = true;
            this.chkReunion.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkReunion.Location = new System.Drawing.Point(62, 378);
            this.chkReunion.Name = "chkReunion";
            this.chkReunion.Size = new System.Drawing.Size(15, 14);
            this.chkReunion.TabIndex = 2;
            this.chkReunion.UseVisualStyleBackColor = true;
            this.chkReunion.CheckedChanged += new System.EventHandler(this.chkReunion_CheckedChanged);
            // 
            // chkCorreoPublico
            // 
            this.chkCorreoPublico.AutoSize = true;
            this.chkCorreoPublico.Checked = true;
            this.chkCorreoPublico.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkCorreoPublico.Location = new System.Drawing.Point(62, 147);
            this.chkCorreoPublico.Name = "chkCorreoPublico";
            this.chkCorreoPublico.Size = new System.Drawing.Size(15, 14);
            this.chkCorreoPublico.TabIndex = 2;
            this.chkCorreoPublico.UseVisualStyleBackColor = true;
            this.chkCorreoPublico.CheckedChanged += new System.EventHandler(this.chkCorreoPublico_CheckedChanged);
            // 
            // chkCorreo
            // 
            this.chkCorreo.AutoSize = true;
            this.chkCorreo.Checked = true;
            this.chkCorreo.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkCorreo.Location = new System.Drawing.Point(62, 62);
            this.chkCorreo.Name = "chkCorreo";
            this.chkCorreo.Size = new System.Drawing.Size(15, 14);
            this.chkCorreo.TabIndex = 2;
            this.chkCorreo.UseVisualStyleBackColor = true;
            this.chkCorreo.CheckedChanged += new System.EventHandler(this.chkCorreo_CheckedChanged);
            // 
            // grpTarea
            // 
            this.grpTarea.Controls.Add(this.radTareaRecordatorio);
            this.grpTarea.Controls.Add(this.radTareaModificacion);
            this.grpTarea.Controls.Add(this.radTareaCreacion);
            this.grpTarea.Location = new System.Drawing.Point(83, 492);
            this.grpTarea.Name = "grpTarea";
            this.grpTarea.Size = new System.Drawing.Size(511, 55);
            this.grpTarea.TabIndex = 1;
            this.grpTarea.TabStop = false;
            this.grpTarea.Text = "Tareas";
            // 
            // radTareaRecordatorio
            // 
            this.radTareaRecordatorio.AutoSize = true;
            this.radTareaRecordatorio.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radTareaRecordatorio.Location = new System.Drawing.Point(393, 19);
            this.radTareaRecordatorio.Name = "radTareaRecordatorio";
            this.radTareaRecordatorio.Size = new System.Drawing.Size(86, 17);
            this.radTareaRecordatorio.TabIndex = 0;
            this.radTareaRecordatorio.Tag = "3";
            this.radTareaRecordatorio.Text = "Recordatorio";
            this.radTareaRecordatorio.UseVisualStyleBackColor = true;
            // 
            // radTareaModificacion
            // 
            this.radTareaModificacion.AutoSize = true;
            this.radTareaModificacion.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radTareaModificacion.Location = new System.Drawing.Point(208, 19);
            this.radTareaModificacion.Name = "radTareaModificacion";
            this.radTareaModificacion.Size = new System.Drawing.Size(116, 17);
            this.radTareaModificacion.TabIndex = 0;
            this.radTareaModificacion.Tag = "2";
            this.radTareaModificacion.Text = "Ultima modificación";
            this.radTareaModificacion.UseVisualStyleBackColor = true;
            // 
            // radTareaCreacion
            // 
            this.radTareaCreacion.AutoSize = true;
            this.radTareaCreacion.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radTareaCreacion.Checked = true;
            this.radTareaCreacion.Location = new System.Drawing.Point(72, 19);
            this.radTareaCreacion.Name = "radTareaCreacion";
            this.radTareaCreacion.Size = new System.Drawing.Size(67, 17);
            this.radTareaCreacion.TabIndex = 0;
            this.radTareaCreacion.TabStop = true;
            this.radTareaCreacion.Tag = "1";
            this.radTareaCreacion.Text = "Creacion";
            this.radTareaCreacion.UseVisualStyleBackColor = true;
            // 
            // grpNota
            // 
            this.grpNota.Controls.Add(this.radNotaModificacion);
            this.grpNota.Controls.Add(this.radNotaCreacion);
            this.grpNota.Enabled = false;
            this.grpNota.Location = new System.Drawing.Point(83, 431);
            this.grpNota.Name = "grpNota";
            this.grpNota.Size = new System.Drawing.Size(511, 55);
            this.grpNota.TabIndex = 1;
            this.grpNota.TabStop = false;
            this.grpNota.Text = "Notas";
            // 
            // radNotaModificacion
            // 
            this.radNotaModificacion.AutoSize = true;
            this.radNotaModificacion.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radNotaModificacion.Location = new System.Drawing.Point(208, 19);
            this.radNotaModificacion.Name = "radNotaModificacion";
            this.radNotaModificacion.Size = new System.Drawing.Size(116, 17);
            this.radNotaModificacion.TabIndex = 0;
            this.radNotaModificacion.Tag = "2";
            this.radNotaModificacion.Text = "Ultima modificación";
            this.radNotaModificacion.UseVisualStyleBackColor = true;
            // 
            // radNotaCreacion
            // 
            this.radNotaCreacion.AutoSize = true;
            this.radNotaCreacion.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radNotaCreacion.Checked = true;
            this.radNotaCreacion.Location = new System.Drawing.Point(72, 19);
            this.radNotaCreacion.Name = "radNotaCreacion";
            this.radNotaCreacion.Size = new System.Drawing.Size(67, 17);
            this.radNotaCreacion.TabIndex = 0;
            this.radNotaCreacion.TabStop = true;
            this.radNotaCreacion.Tag = "1";
            this.radNotaCreacion.Text = "Creacion";
            this.radNotaCreacion.UseVisualStyleBackColor = true;
            // 
            // grpContacto
            // 
            this.grpContacto.Controls.Add(this.radContactoRecordatorio);
            this.grpContacto.Controls.Add(this.radContactoModificacion);
            this.grpContacto.Controls.Add(this.radContactoCreacion);
            this.grpContacto.Enabled = false;
            this.grpContacto.Location = new System.Drawing.Point(83, 285);
            this.grpContacto.Name = "grpContacto";
            this.grpContacto.Size = new System.Drawing.Size(511, 55);
            this.grpContacto.TabIndex = 1;
            this.grpContacto.TabStop = false;
            this.grpContacto.Text = "Contactos";
            // 
            // radContactoRecordatorio
            // 
            this.radContactoRecordatorio.AutoSize = true;
            this.radContactoRecordatorio.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radContactoRecordatorio.Location = new System.Drawing.Point(393, 19);
            this.radContactoRecordatorio.Name = "radContactoRecordatorio";
            this.radContactoRecordatorio.Size = new System.Drawing.Size(86, 17);
            this.radContactoRecordatorio.TabIndex = 0;
            this.radContactoRecordatorio.Tag = "3";
            this.radContactoRecordatorio.Text = "Recordatorio";
            this.radContactoRecordatorio.UseVisualStyleBackColor = true;
            // 
            // radContactoModificacion
            // 
            this.radContactoModificacion.AutoSize = true;
            this.radContactoModificacion.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radContactoModificacion.Location = new System.Drawing.Point(208, 19);
            this.radContactoModificacion.Name = "radContactoModificacion";
            this.radContactoModificacion.Size = new System.Drawing.Size(116, 17);
            this.radContactoModificacion.TabIndex = 0;
            this.radContactoModificacion.Tag = "2";
            this.radContactoModificacion.Text = "Ultima modificación";
            this.radContactoModificacion.UseVisualStyleBackColor = true;
            // 
            // radContactoCreacion
            // 
            this.radContactoCreacion.AutoSize = true;
            this.radContactoCreacion.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radContactoCreacion.Checked = true;
            this.radContactoCreacion.Location = new System.Drawing.Point(72, 19);
            this.radContactoCreacion.Name = "radContactoCreacion";
            this.radContactoCreacion.Size = new System.Drawing.Size(67, 17);
            this.radContactoCreacion.TabIndex = 0;
            this.radContactoCreacion.TabStop = true;
            this.radContactoCreacion.Tag = "1";
            this.radContactoCreacion.Text = "Creacion";
            this.radContactoCreacion.UseVisualStyleBackColor = true;
            // 
            // grpCita
            // 
            this.grpCita.Controls.Add(this.radCitaRespuesta);
            this.grpCita.Controls.Add(this.radCitaModificacion);
            this.grpCita.Controls.Add(this.radCitaFin);
            this.grpCita.Controls.Add(this.radCitaInicio);
            this.grpCita.Controls.Add(this.radCitaCreacion);
            this.grpCita.Location = new System.Drawing.Point(83, 200);
            this.grpCita.Name = "grpCita";
            this.grpCita.Size = new System.Drawing.Size(511, 79);
            this.grpCita.TabIndex = 1;
            this.grpCita.TabStop = false;
            this.grpCita.Text = "Citas";
            // 
            // radCitaRespuesta
            // 
            this.radCitaRespuesta.AutoSize = true;
            this.radCitaRespuesta.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radCitaRespuesta.Location = new System.Drawing.Point(393, 19);
            this.radCitaRespuesta.Name = "radCitaRespuesta";
            this.radCitaRespuesta.Size = new System.Drawing.Size(76, 17);
            this.radCitaRespuesta.TabIndex = 0;
            this.radCitaRespuesta.Tag = "3";
            this.radCitaRespuesta.Text = "Respuesta";
            this.radCitaRespuesta.UseVisualStyleBackColor = true;
            // 
            // radCitaModificacion
            // 
            this.radCitaModificacion.AutoSize = true;
            this.radCitaModificacion.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radCitaModificacion.Location = new System.Drawing.Point(208, 19);
            this.radCitaModificacion.Name = "radCitaModificacion";
            this.radCitaModificacion.Size = new System.Drawing.Size(116, 17);
            this.radCitaModificacion.TabIndex = 0;
            this.radCitaModificacion.Tag = "2";
            this.radCitaModificacion.Text = "Ultima modificación";
            this.radCitaModificacion.UseVisualStyleBackColor = true;
            // 
            // radCitaFin
            // 
            this.radCitaFin.AutoSize = true;
            this.radCitaFin.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radCitaFin.Location = new System.Drawing.Point(285, 42);
            this.radCitaFin.Name = "radCitaFin";
            this.radCitaFin.Size = new System.Drawing.Size(39, 17);
            this.radCitaFin.TabIndex = 0;
            this.radCitaFin.Tag = "5";
            this.radCitaFin.Text = "Fin";
            this.radCitaFin.UseVisualStyleBackColor = true;
            // 
            // radCitaInicio
            // 
            this.radCitaInicio.AutoSize = true;
            this.radCitaInicio.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radCitaInicio.Checked = true;
            this.radCitaInicio.Location = new System.Drawing.Point(89, 42);
            this.radCitaInicio.Name = "radCitaInicio";
            this.radCitaInicio.Size = new System.Drawing.Size(50, 17);
            this.radCitaInicio.TabIndex = 0;
            this.radCitaInicio.TabStop = true;
            this.radCitaInicio.Tag = "4";
            this.radCitaInicio.Text = "Inicio";
            this.radCitaInicio.UseVisualStyleBackColor = true;
            // 
            // radCitaCreacion
            // 
            this.radCitaCreacion.AutoSize = true;
            this.radCitaCreacion.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radCitaCreacion.Location = new System.Drawing.Point(72, 19);
            this.radCitaCreacion.Name = "radCitaCreacion";
            this.radCitaCreacion.Size = new System.Drawing.Size(67, 17);
            this.radCitaCreacion.TabIndex = 0;
            this.radCitaCreacion.Tag = "1";
            this.radCitaCreacion.Text = "Creacion";
            this.radCitaCreacion.UseVisualStyleBackColor = true;
            // 
            // grpReunion
            // 
            this.grpReunion.Controls.Add(this.radReunionRecordatorio);
            this.grpReunion.Controls.Add(this.radReunionRecepcion);
            this.grpReunion.Controls.Add(this.radReunionModificacion);
            this.grpReunion.Controls.Add(this.radReunionCaducidad);
            this.grpReunion.Controls.Add(this.radReunionEntregaDiferida);
            this.grpReunion.Controls.Add(this.radReunionCreacion);
            this.grpReunion.Location = new System.Drawing.Point(83, 346);
            this.grpReunion.Name = "grpReunion";
            this.grpReunion.Size = new System.Drawing.Size(511, 79);
            this.grpReunion.TabIndex = 1;
            this.grpReunion.TabStop = false;
            this.grpReunion.Text = "Reuniones";
            // 
            // radReunionRecordatorio
            // 
            this.radReunionRecordatorio.AutoSize = true;
            this.radReunionRecordatorio.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radReunionRecordatorio.Location = new System.Drawing.Point(384, 42);
            this.radReunionRecordatorio.Name = "radReunionRecordatorio";
            this.radReunionRecordatorio.Size = new System.Drawing.Size(86, 17);
            this.radReunionRecordatorio.TabIndex = 0;
            this.radReunionRecordatorio.Tag = "6";
            this.radReunionRecordatorio.Text = "Recordatorio";
            this.radReunionRecordatorio.UseVisualStyleBackColor = true;
            // 
            // radReunionRecepcion
            // 
            this.radReunionRecepcion.AutoSize = true;
            this.radReunionRecepcion.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radReunionRecepcion.Checked = true;
            this.radReunionRecepcion.Location = new System.Drawing.Point(393, 19);
            this.radReunionRecepcion.Name = "radReunionRecepcion";
            this.radReunionRecepcion.Size = new System.Drawing.Size(77, 17);
            this.radReunionRecepcion.TabIndex = 0;
            this.radReunionRecepcion.TabStop = true;
            this.radReunionRecepcion.Tag = "3";
            this.radReunionRecepcion.Text = "Recepción";
            this.radReunionRecepcion.UseVisualStyleBackColor = true;
            // 
            // radReunionModificacion
            // 
            this.radReunionModificacion.AutoSize = true;
            this.radReunionModificacion.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radReunionModificacion.Location = new System.Drawing.Point(208, 19);
            this.radReunionModificacion.Name = "radReunionModificacion";
            this.radReunionModificacion.Size = new System.Drawing.Size(116, 17);
            this.radReunionModificacion.TabIndex = 0;
            this.radReunionModificacion.Tag = "2";
            this.radReunionModificacion.Text = "Ultima modificación";
            this.radReunionModificacion.UseVisualStyleBackColor = true;
            // 
            // radReunionCaducidad
            // 
            this.radReunionCaducidad.AutoSize = true;
            this.radReunionCaducidad.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radReunionCaducidad.Location = new System.Drawing.Point(248, 42);
            this.radReunionCaducidad.Name = "radReunionCaducidad";
            this.radReunionCaducidad.Size = new System.Drawing.Size(76, 17);
            this.radReunionCaducidad.TabIndex = 0;
            this.radReunionCaducidad.Tag = "5";
            this.radReunionCaducidad.Text = "Caducidad";
            this.radReunionCaducidad.UseVisualStyleBackColor = true;
            // 
            // radReunionEntregaDiferida
            // 
            this.radReunionEntregaDiferida.AutoSize = true;
            this.radReunionEntregaDiferida.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radReunionEntregaDiferida.Location = new System.Drawing.Point(40, 42);
            this.radReunionEntregaDiferida.Name = "radReunionEntregaDiferida";
            this.radReunionEntregaDiferida.Size = new System.Drawing.Size(99, 17);
            this.radReunionEntregaDiferida.TabIndex = 0;
            this.radReunionEntregaDiferida.Tag = "4";
            this.radReunionEntregaDiferida.Text = "Entrega diferida";
            this.radReunionEntregaDiferida.UseVisualStyleBackColor = true;
            // 
            // radReunionCreacion
            // 
            this.radReunionCreacion.AutoSize = true;
            this.radReunionCreacion.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radReunionCreacion.Location = new System.Drawing.Point(72, 19);
            this.radReunionCreacion.Name = "radReunionCreacion";
            this.radReunionCreacion.Size = new System.Drawing.Size(67, 17);
            this.radReunionCreacion.TabIndex = 0;
            this.radReunionCreacion.Tag = "1";
            this.radReunionCreacion.Text = "Creacion";
            this.radReunionCreacion.UseVisualStyleBackColor = true;
            // 
            // grpCorreoPublico
            // 
            this.grpCorreoPublico.Controls.Add(this.radCorreoPublicoRecordatorio);
            this.grpCorreoPublico.Controls.Add(this.radCorreoPublicoRecepcion);
            this.grpCorreoPublico.Controls.Add(this.radCorreoPublicoModificacion);
            this.grpCorreoPublico.Controls.Add(this.radCorreoPublicoCaducidad);
            this.grpCorreoPublico.Controls.Add(this.radCorreoPublicoCreacion);
            this.grpCorreoPublico.Location = new System.Drawing.Point(83, 115);
            this.grpCorreoPublico.Name = "grpCorreoPublico";
            this.grpCorreoPublico.Size = new System.Drawing.Size(511, 79);
            this.grpCorreoPublico.TabIndex = 1;
            this.grpCorreoPublico.TabStop = false;
            this.grpCorreoPublico.Text = "Correos públicos";
            // 
            // radCorreoPublicoRecordatorio
            // 
            this.radCorreoPublicoRecordatorio.AutoSize = true;
            this.radCorreoPublicoRecordatorio.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radCorreoPublicoRecordatorio.Location = new System.Drawing.Point(238, 42);
            this.radCorreoPublicoRecordatorio.Name = "radCorreoPublicoRecordatorio";
            this.radCorreoPublicoRecordatorio.Size = new System.Drawing.Size(86, 17);
            this.radCorreoPublicoRecordatorio.TabIndex = 0;
            this.radCorreoPublicoRecordatorio.Tag = "5";
            this.radCorreoPublicoRecordatorio.Text = "Recordatorio";
            this.radCorreoPublicoRecordatorio.UseVisualStyleBackColor = true;
            // 
            // radCorreoPublicoRecepcion
            // 
            this.radCorreoPublicoRecepcion.AutoSize = true;
            this.radCorreoPublicoRecepcion.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radCorreoPublicoRecepcion.Checked = true;
            this.radCorreoPublicoRecepcion.Location = new System.Drawing.Point(393, 19);
            this.radCorreoPublicoRecepcion.Name = "radCorreoPublicoRecepcion";
            this.radCorreoPublicoRecepcion.Size = new System.Drawing.Size(77, 17);
            this.radCorreoPublicoRecepcion.TabIndex = 0;
            this.radCorreoPublicoRecepcion.TabStop = true;
            this.radCorreoPublicoRecepcion.Tag = "3";
            this.radCorreoPublicoRecepcion.Text = "Recepción";
            this.radCorreoPublicoRecepcion.UseVisualStyleBackColor = true;
            // 
            // radCorreoPublicoModificacion
            // 
            this.radCorreoPublicoModificacion.AutoSize = true;
            this.radCorreoPublicoModificacion.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radCorreoPublicoModificacion.Location = new System.Drawing.Point(208, 19);
            this.radCorreoPublicoModificacion.Name = "radCorreoPublicoModificacion";
            this.radCorreoPublicoModificacion.Size = new System.Drawing.Size(116, 17);
            this.radCorreoPublicoModificacion.TabIndex = 0;
            this.radCorreoPublicoModificacion.Tag = "2";
            this.radCorreoPublicoModificacion.Text = "Ultima modificación";
            this.radCorreoPublicoModificacion.UseVisualStyleBackColor = true;
            // 
            // radCorreoPublicoCaducidad
            // 
            this.radCorreoPublicoCaducidad.AutoSize = true;
            this.radCorreoPublicoCaducidad.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radCorreoPublicoCaducidad.Location = new System.Drawing.Point(63, 42);
            this.radCorreoPublicoCaducidad.Name = "radCorreoPublicoCaducidad";
            this.radCorreoPublicoCaducidad.Size = new System.Drawing.Size(76, 17);
            this.radCorreoPublicoCaducidad.TabIndex = 0;
            this.radCorreoPublicoCaducidad.Tag = "4";
            this.radCorreoPublicoCaducidad.Text = "Caducidad";
            this.radCorreoPublicoCaducidad.UseVisualStyleBackColor = true;
            // 
            // radCorreoPublicoCreacion
            // 
            this.radCorreoPublicoCreacion.AutoSize = true;
            this.radCorreoPublicoCreacion.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radCorreoPublicoCreacion.Location = new System.Drawing.Point(72, 19);
            this.radCorreoPublicoCreacion.Name = "radCorreoPublicoCreacion";
            this.radCorreoPublicoCreacion.Size = new System.Drawing.Size(67, 17);
            this.radCorreoPublicoCreacion.TabIndex = 0;
            this.radCorreoPublicoCreacion.Tag = "1";
            this.radCorreoPublicoCreacion.Text = "Creacion";
            this.radCorreoPublicoCreacion.UseVisualStyleBackColor = true;
            // 
            // grpCorreo
            // 
            this.grpCorreo.Controls.Add(this.radCorreoRecordatorio);
            this.grpCorreo.Controls.Add(this.radCorreoRecepcion);
            this.grpCorreo.Controls.Add(this.radCorreoModificacion);
            this.grpCorreo.Controls.Add(this.radCorreoCaducidad);
            this.grpCorreo.Controls.Add(this.radCorreoEtnregaDiferida);
            this.grpCorreo.Controls.Add(this.radCorreoCreacion);
            this.grpCorreo.Location = new System.Drawing.Point(83, 30);
            this.grpCorreo.Name = "grpCorreo";
            this.grpCorreo.Size = new System.Drawing.Size(511, 79);
            this.grpCorreo.TabIndex = 1;
            this.grpCorreo.TabStop = false;
            this.grpCorreo.Text = "Correos";
            // 
            // radCorreoRecordatorio
            // 
            this.radCorreoRecordatorio.AutoSize = true;
            this.radCorreoRecordatorio.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radCorreoRecordatorio.Location = new System.Drawing.Point(384, 42);
            this.radCorreoRecordatorio.Name = "radCorreoRecordatorio";
            this.radCorreoRecordatorio.Size = new System.Drawing.Size(86, 17);
            this.radCorreoRecordatorio.TabIndex = 0;
            this.radCorreoRecordatorio.Tag = "6";
            this.radCorreoRecordatorio.Text = "Recordatorio";
            this.radCorreoRecordatorio.UseVisualStyleBackColor = true;
            // 
            // radCorreoRecepcion
            // 
            this.radCorreoRecepcion.AutoSize = true;
            this.radCorreoRecepcion.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radCorreoRecepcion.Checked = true;
            this.radCorreoRecepcion.Location = new System.Drawing.Point(393, 19);
            this.radCorreoRecepcion.Name = "radCorreoRecepcion";
            this.radCorreoRecepcion.Size = new System.Drawing.Size(77, 17);
            this.radCorreoRecepcion.TabIndex = 0;
            this.radCorreoRecepcion.TabStop = true;
            this.radCorreoRecepcion.Tag = "3";
            this.radCorreoRecepcion.Text = "Recepción";
            this.radCorreoRecepcion.UseVisualStyleBackColor = true;
            // 
            // radCorreoModificacion
            // 
            this.radCorreoModificacion.AutoSize = true;
            this.radCorreoModificacion.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radCorreoModificacion.Location = new System.Drawing.Point(208, 19);
            this.radCorreoModificacion.Name = "radCorreoModificacion";
            this.radCorreoModificacion.Size = new System.Drawing.Size(116, 17);
            this.radCorreoModificacion.TabIndex = 0;
            this.radCorreoModificacion.Tag = "2";
            this.radCorreoModificacion.Text = "Ultima modificación";
            this.radCorreoModificacion.UseVisualStyleBackColor = true;
            // 
            // radCorreoCaducidad
            // 
            this.radCorreoCaducidad.AutoSize = true;
            this.radCorreoCaducidad.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radCorreoCaducidad.Location = new System.Drawing.Point(248, 42);
            this.radCorreoCaducidad.Name = "radCorreoCaducidad";
            this.radCorreoCaducidad.Size = new System.Drawing.Size(76, 17);
            this.radCorreoCaducidad.TabIndex = 0;
            this.radCorreoCaducidad.Tag = "5";
            this.radCorreoCaducidad.Text = "Caducidad";
            this.radCorreoCaducidad.UseVisualStyleBackColor = true;
            // 
            // radCorreoEtnregaDiferida
            // 
            this.radCorreoEtnregaDiferida.AutoSize = true;
            this.radCorreoEtnregaDiferida.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radCorreoEtnregaDiferida.Location = new System.Drawing.Point(40, 42);
            this.radCorreoEtnregaDiferida.Name = "radCorreoEtnregaDiferida";
            this.radCorreoEtnregaDiferida.Size = new System.Drawing.Size(99, 17);
            this.radCorreoEtnregaDiferida.TabIndex = 0;
            this.radCorreoEtnregaDiferida.Tag = "4";
            this.radCorreoEtnregaDiferida.Text = "Entrega diferida";
            this.radCorreoEtnregaDiferida.UseVisualStyleBackColor = true;
            // 
            // radCorreoCreacion
            // 
            this.radCorreoCreacion.AutoSize = true;
            this.radCorreoCreacion.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radCorreoCreacion.Location = new System.Drawing.Point(72, 19);
            this.radCorreoCreacion.Name = "radCorreoCreacion";
            this.radCorreoCreacion.Size = new System.Drawing.Size(67, 17);
            this.radCorreoCreacion.TabIndex = 0;
            this.radCorreoCreacion.Tag = "1";
            this.radCorreoCreacion.Text = "Creacion";
            this.radCorreoCreacion.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(47, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Archivar según:";
            // 
            // tabFechas2
            // 
            this.tabFechas2.Controls.Add(this.grpIntercambio);
            this.tabFechas2.Controls.Add(this.chkIntercambio);
            this.tabFechas2.Controls.Add(this.chkSolicitudAct);
            this.tabFechas2.Controls.Add(this.chkSolicitudRech);
            this.tabFechas2.Controls.Add(this.chkSolicitudAcept);
            this.tabFechas2.Controls.Add(this.chkSolicitud);
            this.tabFechas2.Controls.Add(this.grpSolicitudAct);
            this.tabFechas2.Controls.Add(this.grpSolicitudRech);
            this.tabFechas2.Controls.Add(this.grpSolicitudAcept);
            this.tabFechas2.Controls.Add(this.grpSolicitud);
            this.tabFechas2.Controls.Add(this.chkDiario);
            this.tabFechas2.Controls.Add(this.btnDefectoFechas2);
            this.tabFechas2.Controls.Add(this.grpDiario);
            this.tabFechas2.Controls.Add(this.chkInforme);
            this.tabFechas2.Controls.Add(this.grpInforme);
            this.tabFechas2.Controls.Add(this.label3);
            this.tabFechas2.Location = new System.Drawing.Point(4, 22);
            this.tabFechas2.Name = "tabFechas2";
            this.tabFechas2.Size = new System.Drawing.Size(623, 601);
            this.tabFechas2.TabIndex = 2;
            this.tabFechas2.Text = "Filtro de fechas 2";
            this.tabFechas2.UseVisualStyleBackColor = true;
            // 
            // grpIntercambio
            // 
            this.grpIntercambio.Controls.Add(this.radIntercambioRecordatorio);
            this.grpIntercambio.Controls.Add(this.radIntercambioRecepcion);
            this.grpIntercambio.Controls.Add(this.radIntercambioModificacion);
            this.grpIntercambio.Controls.Add(this.radIntercambioCaducidad);
            this.grpIntercambio.Controls.Add(this.radIntercambioEntregaDiferida);
            this.grpIntercambio.Controls.Add(this.radIntercambioCreacion);
            this.grpIntercambio.Location = new System.Drawing.Point(83, 395);
            this.grpIntercambio.Name = "grpIntercambio";
            this.grpIntercambio.Size = new System.Drawing.Size(511, 79);
            this.grpIntercambio.TabIndex = 23;
            this.grpIntercambio.TabStop = false;
            this.grpIntercambio.Text = "Solicitud de Intercambio";
            // 
            // radIntercambioRecordatorio
            // 
            this.radIntercambioRecordatorio.AutoSize = true;
            this.radIntercambioRecordatorio.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radIntercambioRecordatorio.Location = new System.Drawing.Point(384, 42);
            this.radIntercambioRecordatorio.Name = "radIntercambioRecordatorio";
            this.radIntercambioRecordatorio.Size = new System.Drawing.Size(86, 17);
            this.radIntercambioRecordatorio.TabIndex = 0;
            this.radIntercambioRecordatorio.Tag = "6";
            this.radIntercambioRecordatorio.Text = "Recordatorio";
            this.radIntercambioRecordatorio.UseVisualStyleBackColor = true;
            // 
            // radIntercambioRecepcion
            // 
            this.radIntercambioRecepcion.AutoSize = true;
            this.radIntercambioRecepcion.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radIntercambioRecepcion.Checked = true;
            this.radIntercambioRecepcion.Location = new System.Drawing.Point(393, 19);
            this.radIntercambioRecepcion.Name = "radIntercambioRecepcion";
            this.radIntercambioRecepcion.Size = new System.Drawing.Size(77, 17);
            this.radIntercambioRecepcion.TabIndex = 0;
            this.radIntercambioRecepcion.TabStop = true;
            this.radIntercambioRecepcion.Tag = "3";
            this.radIntercambioRecepcion.Text = "Recepción";
            this.radIntercambioRecepcion.UseVisualStyleBackColor = true;
            // 
            // radIntercambioModificacion
            // 
            this.radIntercambioModificacion.AutoSize = true;
            this.radIntercambioModificacion.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radIntercambioModificacion.Location = new System.Drawing.Point(208, 19);
            this.radIntercambioModificacion.Name = "radIntercambioModificacion";
            this.radIntercambioModificacion.Size = new System.Drawing.Size(116, 17);
            this.radIntercambioModificacion.TabIndex = 0;
            this.radIntercambioModificacion.Tag = "2";
            this.radIntercambioModificacion.Text = "Ultima modificación";
            this.radIntercambioModificacion.UseVisualStyleBackColor = true;
            // 
            // radIntercambioCaducidad
            // 
            this.radIntercambioCaducidad.AutoSize = true;
            this.radIntercambioCaducidad.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radIntercambioCaducidad.Location = new System.Drawing.Point(248, 42);
            this.radIntercambioCaducidad.Name = "radIntercambioCaducidad";
            this.radIntercambioCaducidad.Size = new System.Drawing.Size(76, 17);
            this.radIntercambioCaducidad.TabIndex = 0;
            this.radIntercambioCaducidad.Tag = "5";
            this.radIntercambioCaducidad.Text = "Caducidad";
            this.radIntercambioCaducidad.UseVisualStyleBackColor = true;
            // 
            // radIntercambioEntregaDiferida
            // 
            this.radIntercambioEntregaDiferida.AutoSize = true;
            this.radIntercambioEntregaDiferida.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radIntercambioEntregaDiferida.Location = new System.Drawing.Point(40, 42);
            this.radIntercambioEntregaDiferida.Name = "radIntercambioEntregaDiferida";
            this.radIntercambioEntregaDiferida.Size = new System.Drawing.Size(99, 17);
            this.radIntercambioEntregaDiferida.TabIndex = 0;
            this.radIntercambioEntregaDiferida.Tag = "4";
            this.radIntercambioEntregaDiferida.Text = "Entrega diferida";
            this.radIntercambioEntregaDiferida.UseVisualStyleBackColor = true;
            // 
            // radIntercambioCreacion
            // 
            this.radIntercambioCreacion.AutoSize = true;
            this.radIntercambioCreacion.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radIntercambioCreacion.Location = new System.Drawing.Point(72, 19);
            this.radIntercambioCreacion.Name = "radIntercambioCreacion";
            this.radIntercambioCreacion.Size = new System.Drawing.Size(67, 17);
            this.radIntercambioCreacion.TabIndex = 0;
            this.radIntercambioCreacion.Tag = "1";
            this.radIntercambioCreacion.Text = "Creacion";
            this.radIntercambioCreacion.UseVisualStyleBackColor = true;
            // 
            // chkIntercambio
            // 
            this.chkIntercambio.AutoSize = true;
            this.chkIntercambio.Checked = true;
            this.chkIntercambio.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIntercambio.Location = new System.Drawing.Point(62, 427);
            this.chkIntercambio.Name = "chkIntercambio";
            this.chkIntercambio.Size = new System.Drawing.Size(15, 14);
            this.chkIntercambio.TabIndex = 22;
            this.chkIntercambio.UseVisualStyleBackColor = true;
            this.chkIntercambio.CheckedChanged += new System.EventHandler(this.chkIntercambio_CheckedChanged);
            // 
            // chkSolicitudAct
            // 
            this.chkSolicitudAct.AutoSize = true;
            this.chkSolicitudAct.Checked = true;
            this.chkSolicitudAct.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSolicitudAct.Location = new System.Drawing.Point(62, 354);
            this.chkSolicitudAct.Name = "chkSolicitudAct";
            this.chkSolicitudAct.Size = new System.Drawing.Size(15, 14);
            this.chkSolicitudAct.TabIndex = 22;
            this.chkSolicitudAct.UseVisualStyleBackColor = true;
            this.chkSolicitudAct.CheckedChanged += new System.EventHandler(this.chkSolicitudAct_CheckedChanged);
            // 
            // chkSolicitudRech
            // 
            this.chkSolicitudRech.AutoSize = true;
            this.chkSolicitudRech.Checked = true;
            this.chkSolicitudRech.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSolicitudRech.Location = new System.Drawing.Point(62, 293);
            this.chkSolicitudRech.Name = "chkSolicitudRech";
            this.chkSolicitudRech.Size = new System.Drawing.Size(15, 14);
            this.chkSolicitudRech.TabIndex = 22;
            this.chkSolicitudRech.UseVisualStyleBackColor = true;
            this.chkSolicitudRech.CheckedChanged += new System.EventHandler(this.chkSolicitudRech_CheckedChanged);
            // 
            // chkSolicitudAcept
            // 
            this.chkSolicitudAcept.AutoSize = true;
            this.chkSolicitudAcept.Checked = true;
            this.chkSolicitudAcept.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSolicitudAcept.Location = new System.Drawing.Point(62, 232);
            this.chkSolicitudAcept.Name = "chkSolicitudAcept";
            this.chkSolicitudAcept.Size = new System.Drawing.Size(15, 14);
            this.chkSolicitudAcept.TabIndex = 22;
            this.chkSolicitudAcept.UseVisualStyleBackColor = true;
            this.chkSolicitudAcept.CheckedChanged += new System.EventHandler(this.chkSolicitudAcept_CheckedChanged);
            // 
            // chkSolicitud
            // 
            this.chkSolicitud.AutoSize = true;
            this.chkSolicitud.Checked = true;
            this.chkSolicitud.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSolicitud.Location = new System.Drawing.Point(62, 171);
            this.chkSolicitud.Name = "chkSolicitud";
            this.chkSolicitud.Size = new System.Drawing.Size(15, 14);
            this.chkSolicitud.TabIndex = 22;
            this.chkSolicitud.UseVisualStyleBackColor = true;
            this.chkSolicitud.CheckedChanged += new System.EventHandler(this.chkSolicitud_CheckedChanged);
            // 
            // grpSolicitudAct
            // 
            this.grpSolicitudAct.Controls.Add(this.radSolicitudActModificacion);
            this.grpSolicitudAct.Controls.Add(this.radSolicitudActCreacion);
            this.grpSolicitudAct.Location = new System.Drawing.Point(83, 334);
            this.grpSolicitudAct.Name = "grpSolicitudAct";
            this.grpSolicitudAct.Size = new System.Drawing.Size(511, 55);
            this.grpSolicitudAct.TabIndex = 21;
            this.grpSolicitudAct.TabStop = false;
            this.grpSolicitudAct.Text = "Solicitudes Actualizadas";
            // 
            // radSolicitudActModificacion
            // 
            this.radSolicitudActModificacion.AutoSize = true;
            this.radSolicitudActModificacion.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radSolicitudActModificacion.Location = new System.Drawing.Point(208, 19);
            this.radSolicitudActModificacion.Name = "radSolicitudActModificacion";
            this.radSolicitudActModificacion.Size = new System.Drawing.Size(116, 17);
            this.radSolicitudActModificacion.TabIndex = 0;
            this.radSolicitudActModificacion.Tag = "2";
            this.radSolicitudActModificacion.Text = "Ultima modificación";
            this.radSolicitudActModificacion.UseVisualStyleBackColor = true;
            // 
            // radSolicitudActCreacion
            // 
            this.radSolicitudActCreacion.AutoSize = true;
            this.radSolicitudActCreacion.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radSolicitudActCreacion.Checked = true;
            this.radSolicitudActCreacion.Location = new System.Drawing.Point(72, 19);
            this.radSolicitudActCreacion.Name = "radSolicitudActCreacion";
            this.radSolicitudActCreacion.Size = new System.Drawing.Size(67, 17);
            this.radSolicitudActCreacion.TabIndex = 0;
            this.radSolicitudActCreacion.TabStop = true;
            this.radSolicitudActCreacion.Tag = "1";
            this.radSolicitudActCreacion.Text = "Creacion";
            this.radSolicitudActCreacion.UseVisualStyleBackColor = true;
            // 
            // grpSolicitudRech
            // 
            this.grpSolicitudRech.Controls.Add(this.radSolicitudRechModificacion);
            this.grpSolicitudRech.Controls.Add(this.radSolicitudRechCreacion);
            this.grpSolicitudRech.Location = new System.Drawing.Point(83, 273);
            this.grpSolicitudRech.Name = "grpSolicitudRech";
            this.grpSolicitudRech.Size = new System.Drawing.Size(511, 55);
            this.grpSolicitudRech.TabIndex = 21;
            this.grpSolicitudRech.TabStop = false;
            this.grpSolicitudRech.Text = "Solicitudes Rechazadas";
            // 
            // radSolicitudRechModificacion
            // 
            this.radSolicitudRechModificacion.AutoSize = true;
            this.radSolicitudRechModificacion.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radSolicitudRechModificacion.Location = new System.Drawing.Point(208, 19);
            this.radSolicitudRechModificacion.Name = "radSolicitudRechModificacion";
            this.radSolicitudRechModificacion.Size = new System.Drawing.Size(116, 17);
            this.radSolicitudRechModificacion.TabIndex = 0;
            this.radSolicitudRechModificacion.Tag = "2";
            this.radSolicitudRechModificacion.Text = "Ultima modificación";
            this.radSolicitudRechModificacion.UseVisualStyleBackColor = true;
            // 
            // radSolicitudRechCreacion
            // 
            this.radSolicitudRechCreacion.AutoSize = true;
            this.radSolicitudRechCreacion.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radSolicitudRechCreacion.Checked = true;
            this.radSolicitudRechCreacion.Location = new System.Drawing.Point(72, 19);
            this.radSolicitudRechCreacion.Name = "radSolicitudRechCreacion";
            this.radSolicitudRechCreacion.Size = new System.Drawing.Size(67, 17);
            this.radSolicitudRechCreacion.TabIndex = 0;
            this.radSolicitudRechCreacion.TabStop = true;
            this.radSolicitudRechCreacion.Tag = "1";
            this.radSolicitudRechCreacion.Text = "Creacion";
            this.radSolicitudRechCreacion.UseVisualStyleBackColor = true;
            // 
            // grpSolicitudAcept
            // 
            this.grpSolicitudAcept.Controls.Add(this.radSolicitudAceptModificacion);
            this.grpSolicitudAcept.Controls.Add(this.radSolicitudAceptCreacion);
            this.grpSolicitudAcept.Location = new System.Drawing.Point(83, 212);
            this.grpSolicitudAcept.Name = "grpSolicitudAcept";
            this.grpSolicitudAcept.Size = new System.Drawing.Size(511, 55);
            this.grpSolicitudAcept.TabIndex = 21;
            this.grpSolicitudAcept.TabStop = false;
            this.grpSolicitudAcept.Text = "Solicitudes Aceptadas";
            // 
            // radSolicitudAceptModificacion
            // 
            this.radSolicitudAceptModificacion.AutoSize = true;
            this.radSolicitudAceptModificacion.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radSolicitudAceptModificacion.Location = new System.Drawing.Point(208, 19);
            this.radSolicitudAceptModificacion.Name = "radSolicitudAceptModificacion";
            this.radSolicitudAceptModificacion.Size = new System.Drawing.Size(116, 17);
            this.radSolicitudAceptModificacion.TabIndex = 0;
            this.radSolicitudAceptModificacion.Tag = "2";
            this.radSolicitudAceptModificacion.Text = "Ultima modificación";
            this.radSolicitudAceptModificacion.UseVisualStyleBackColor = true;
            // 
            // radSolicitudAceptCreacion
            // 
            this.radSolicitudAceptCreacion.AutoSize = true;
            this.radSolicitudAceptCreacion.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radSolicitudAceptCreacion.Checked = true;
            this.radSolicitudAceptCreacion.Location = new System.Drawing.Point(72, 19);
            this.radSolicitudAceptCreacion.Name = "radSolicitudAceptCreacion";
            this.radSolicitudAceptCreacion.Size = new System.Drawing.Size(67, 17);
            this.radSolicitudAceptCreacion.TabIndex = 0;
            this.radSolicitudAceptCreacion.TabStop = true;
            this.radSolicitudAceptCreacion.Tag = "1";
            this.radSolicitudAceptCreacion.Text = "Creacion";
            this.radSolicitudAceptCreacion.UseVisualStyleBackColor = true;
            // 
            // grpSolicitud
            // 
            this.grpSolicitud.Controls.Add(this.radSolicitudModificacion);
            this.grpSolicitud.Controls.Add(this.radSolicitudCreacion);
            this.grpSolicitud.Location = new System.Drawing.Point(83, 151);
            this.grpSolicitud.Name = "grpSolicitud";
            this.grpSolicitud.Size = new System.Drawing.Size(511, 55);
            this.grpSolicitud.TabIndex = 21;
            this.grpSolicitud.TabStop = false;
            this.grpSolicitud.Text = "Solicitudes";
            // 
            // radSolicitudModificacion
            // 
            this.radSolicitudModificacion.AutoSize = true;
            this.radSolicitudModificacion.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radSolicitudModificacion.Location = new System.Drawing.Point(208, 19);
            this.radSolicitudModificacion.Name = "radSolicitudModificacion";
            this.radSolicitudModificacion.Size = new System.Drawing.Size(116, 17);
            this.radSolicitudModificacion.TabIndex = 0;
            this.radSolicitudModificacion.Tag = "2";
            this.radSolicitudModificacion.Text = "Ultima modificación";
            this.radSolicitudModificacion.UseVisualStyleBackColor = true;
            // 
            // radSolicitudCreacion
            // 
            this.radSolicitudCreacion.AutoSize = true;
            this.radSolicitudCreacion.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radSolicitudCreacion.Checked = true;
            this.radSolicitudCreacion.Location = new System.Drawing.Point(72, 19);
            this.radSolicitudCreacion.Name = "radSolicitudCreacion";
            this.radSolicitudCreacion.Size = new System.Drawing.Size(67, 17);
            this.radSolicitudCreacion.TabIndex = 0;
            this.radSolicitudCreacion.TabStop = true;
            this.radSolicitudCreacion.Tag = "1";
            this.radSolicitudCreacion.Text = "Creacion";
            this.radSolicitudCreacion.UseVisualStyleBackColor = true;
            // 
            // chkDiario
            // 
            this.chkDiario.AutoSize = true;
            this.chkDiario.Checked = true;
            this.chkDiario.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkDiario.Location = new System.Drawing.Point(62, 110);
            this.chkDiario.Name = "chkDiario";
            this.chkDiario.Size = new System.Drawing.Size(15, 14);
            this.chkDiario.TabIndex = 20;
            this.chkDiario.UseVisualStyleBackColor = true;
            this.chkDiario.CheckedChanged += new System.EventHandler(this.chkDiario_CheckedChanged);
            // 
            // btnDefectoFechas2
            // 
            this.btnDefectoFechas2.Location = new System.Drawing.Point(28, 564);
            this.btnDefectoFechas2.Name = "btnDefectoFechas2";
            this.btnDefectoFechas2.Size = new System.Drawing.Size(75, 23);
            this.btnDefectoFechas2.TabIndex = 19;
            this.btnDefectoFechas2.Text = "Por defecto";
            this.btnDefectoFechas2.UseVisualStyleBackColor = true;
            this.btnDefectoFechas2.Click += new System.EventHandler(this.btnDefectoFechas2_Click);
            // 
            // grpDiario
            // 
            this.grpDiario.Controls.Add(this.radDiarioModificacion);
            this.grpDiario.Controls.Add(this.radDiarioCreacion);
            this.grpDiario.Location = new System.Drawing.Point(83, 90);
            this.grpDiario.Name = "grpDiario";
            this.grpDiario.Size = new System.Drawing.Size(511, 55);
            this.grpDiario.TabIndex = 19;
            this.grpDiario.TabStop = false;
            this.grpDiario.Text = "Diarios";
            // 
            // radDiarioModificacion
            // 
            this.radDiarioModificacion.AutoSize = true;
            this.radDiarioModificacion.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radDiarioModificacion.Location = new System.Drawing.Point(208, 19);
            this.radDiarioModificacion.Name = "radDiarioModificacion";
            this.radDiarioModificacion.Size = new System.Drawing.Size(116, 17);
            this.radDiarioModificacion.TabIndex = 0;
            this.radDiarioModificacion.Tag = "2";
            this.radDiarioModificacion.Text = "Ultima modificación";
            this.radDiarioModificacion.UseVisualStyleBackColor = true;
            // 
            // radDiarioCreacion
            // 
            this.radDiarioCreacion.AutoSize = true;
            this.radDiarioCreacion.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radDiarioCreacion.Checked = true;
            this.radDiarioCreacion.Location = new System.Drawing.Point(72, 19);
            this.radDiarioCreacion.Name = "radDiarioCreacion";
            this.radDiarioCreacion.Size = new System.Drawing.Size(67, 17);
            this.radDiarioCreacion.TabIndex = 0;
            this.radDiarioCreacion.TabStop = true;
            this.radDiarioCreacion.Tag = "1";
            this.radDiarioCreacion.Text = "Creacion";
            this.radDiarioCreacion.UseVisualStyleBackColor = true;
            // 
            // chkInforme
            // 
            this.chkInforme.AutoSize = true;
            this.chkInforme.Checked = true;
            this.chkInforme.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkInforme.Location = new System.Drawing.Point(62, 49);
            this.chkInforme.Name = "chkInforme";
            this.chkInforme.Size = new System.Drawing.Size(15, 14);
            this.chkInforme.TabIndex = 18;
            this.chkInforme.UseVisualStyleBackColor = true;
            this.chkInforme.CheckedChanged += new System.EventHandler(this.chkInforme_CheckedChanged);
            // 
            // grpInforme
            // 
            this.grpInforme.Controls.Add(this.radInformeModificacion);
            this.grpInforme.Controls.Add(this.radInformeCreacion);
            this.grpInforme.Location = new System.Drawing.Point(83, 29);
            this.grpInforme.Name = "grpInforme";
            this.grpInforme.Size = new System.Drawing.Size(511, 55);
            this.grpInforme.TabIndex = 11;
            this.grpInforme.TabStop = false;
            this.grpInforme.Text = "Informes";
            // 
            // radInformeModificacion
            // 
            this.radInformeModificacion.AutoSize = true;
            this.radInformeModificacion.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radInformeModificacion.Location = new System.Drawing.Point(208, 19);
            this.radInformeModificacion.Name = "radInformeModificacion";
            this.radInformeModificacion.Size = new System.Drawing.Size(116, 17);
            this.radInformeModificacion.TabIndex = 0;
            this.radInformeModificacion.Tag = "2";
            this.radInformeModificacion.Text = "Ultima modificación";
            this.radInformeModificacion.UseVisualStyleBackColor = true;
            // 
            // radInformeCreacion
            // 
            this.radInformeCreacion.AutoSize = true;
            this.radInformeCreacion.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radInformeCreacion.Checked = true;
            this.radInformeCreacion.Location = new System.Drawing.Point(72, 19);
            this.radInformeCreacion.Name = "radInformeCreacion";
            this.radInformeCreacion.Size = new System.Drawing.Size(67, 17);
            this.radInformeCreacion.TabIndex = 0;
            this.radInformeCreacion.TabStop = true;
            this.radInformeCreacion.Tag = "1";
            this.radInformeCreacion.Text = "Creacion";
            this.radInformeCreacion.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(47, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Archivar según:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(87, 72);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Muestra";
            // 
            // frmPreferencias
            // 
            this.AcceptButton = this.btnAplicar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(804, 680);
            this.Controls.Add(this.tabPreferencias);
            this.Controls.Add(this.btnAplicar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.KeyPreview = true;
            this.Name = "frmPreferencias";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Preferencias";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmPreferencias_FormClosed);
            this.Load += new System.EventHandler(this.frmPreferencias_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmPreferencias_KeyDown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.grpArchivarSegunImportancia.ResumeLayout(false);
            this.grpArchivarSegunImportancia.PerformLayout();
            this.tabPreferencias.ResumeLayout(false);
            this.tabGenerales.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tabFechas1.ResumeLayout(false);
            this.tabFechas1.PerformLayout();
            this.grpTarea.ResumeLayout(false);
            this.grpTarea.PerformLayout();
            this.grpNota.ResumeLayout(false);
            this.grpNota.PerformLayout();
            this.grpContacto.ResumeLayout(false);
            this.grpContacto.PerformLayout();
            this.grpCita.ResumeLayout(false);
            this.grpCita.PerformLayout();
            this.grpReunion.ResumeLayout(false);
            this.grpReunion.PerformLayout();
            this.grpCorreoPublico.ResumeLayout(false);
            this.grpCorreoPublico.PerformLayout();
            this.grpCorreo.ResumeLayout(false);
            this.grpCorreo.PerformLayout();
            this.tabFechas2.ResumeLayout(false);
            this.tabFechas2.PerformLayout();
            this.grpIntercambio.ResumeLayout(false);
            this.grpIntercambio.PerformLayout();
            this.grpSolicitudAct.ResumeLayout(false);
            this.grpSolicitudAct.PerformLayout();
            this.grpSolicitudRech.ResumeLayout(false);
            this.grpSolicitudRech.PerformLayout();
            this.grpSolicitudAcept.ResumeLayout(false);
            this.grpSolicitudAcept.PerformLayout();
            this.grpSolicitud.ResumeLayout(false);
            this.grpSolicitud.PerformLayout();
            this.grpDiario.ResumeLayout(false);
            this.grpDiario.PerformLayout();
            this.grpInforme.ResumeLayout(false);
            this.grpInforme.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox chkEliminarDirectamente;
        private System.Windows.Forms.Button btnAplicar;
        private System.Windows.Forms.TextBox txtRutaPST;
        private System.Windows.Forms.Label labelRutaPST;
        private System.Windows.Forms.TextBox txtNombreArchivo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtExtension;
        private System.Windows.Forms.Button btnAyuda;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label lblResultado;
        private System.Windows.Forms.CheckBox chkRenombrarRaiz;
        private System.Windows.Forms.CheckBox chkEliminarPSTsVaciosTrasArchiar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox chkArchivarElementosImportantes;
        private System.Windows.Forms.CheckBox chkEliminarCarpetasVaciasDespuesDeArchivar;
        private System.Windows.Forms.CheckBox chkArchivarElementosNoImportantes;
        private System.Windows.Forms.CheckBox chkArchivarElementosNormales;
        private System.Windows.Forms.GroupBox grpArchivarSegunImportancia;
        private System.Windows.Forms.Label lblImportanciaNota;
        private System.Windows.Forms.CheckBox chkArchivarElementosNoLeidos;
        private System.Windows.Forms.TabControl tabPreferencias;
        private System.Windows.Forms.TabPage tabGenerales;
        private System.Windows.Forms.TabPage tabFechas1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox grpCorreo;
        private System.Windows.Forms.RadioButton radCorreoCreacion;
        private System.Windows.Forms.RadioButton radCorreoModificacion;
        private System.Windows.Forms.RadioButton radCorreoRecepcion;
        private System.Windows.Forms.RadioButton radCorreoRecordatorio;
        private System.Windows.Forms.RadioButton radCorreoEtnregaDiferida;
        private System.Windows.Forms.RadioButton radCorreoCaducidad;
        private System.Windows.Forms.GroupBox grpCita;
        private System.Windows.Forms.RadioButton radCitaRespuesta;
        private System.Windows.Forms.RadioButton radCitaModificacion;
        private System.Windows.Forms.RadioButton radCitaFin;
        private System.Windows.Forms.RadioButton radCitaInicio;
        private System.Windows.Forms.RadioButton radCitaCreacion;
        private System.Windows.Forms.GroupBox grpContacto;
        private System.Windows.Forms.RadioButton radContactoRecordatorio;
        private System.Windows.Forms.RadioButton radContactoModificacion;
        private System.Windows.Forms.RadioButton radContactoCreacion;
        private System.Windows.Forms.CheckBox chkCorreo;
        private System.Windows.Forms.CheckBox chkContacto;
        private System.Windows.Forms.CheckBox chkCita;
        private System.Windows.Forms.CheckBox chkReunion;
        private System.Windows.Forms.GroupBox grpReunion;
        private System.Windows.Forms.RadioButton radReunionRecordatorio;
        private System.Windows.Forms.RadioButton radReunionRecepcion;
        private System.Windows.Forms.RadioButton radReunionModificacion;
        private System.Windows.Forms.RadioButton radReunionCaducidad;
        private System.Windows.Forms.RadioButton radReunionEntregaDiferida;
        private System.Windows.Forms.RadioButton radReunionCreacion;
        private System.Windows.Forms.CheckBox chkNota;
        private System.Windows.Forms.GroupBox grpNota;
        private System.Windows.Forms.RadioButton radNotaModificacion;
        private System.Windows.Forms.RadioButton radNotaCreacion;
        private System.Windows.Forms.CheckBox chkTarea;
        private System.Windows.Forms.GroupBox grpTarea;
        private System.Windows.Forms.RadioButton radTareaRecordatorio;
        private System.Windows.Forms.RadioButton radTareaModificacion;
        private System.Windows.Forms.RadioButton radTareaCreacion;
        private System.Windows.Forms.CheckBox chkArchivarElementosSegunImportancia;
        private System.Windows.Forms.Button btnDefectoFechas;
        private System.Windows.Forms.GroupBox grpCorreoPublico;
        private System.Windows.Forms.RadioButton radCorreoPublicoRecordatorio;
        private System.Windows.Forms.RadioButton radCorreoPublicoRecepcion;
        private System.Windows.Forms.RadioButton radCorreoPublicoModificacion;
        private System.Windows.Forms.RadioButton radCorreoPublicoCaducidad;
        private System.Windows.Forms.RadioButton radCorreoPublicoCreacion;
        private System.Windows.Forms.CheckBox chkCorreoPublico;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox chkEliminarArchivoPSTSiVacio;
        private System.Windows.Forms.TabPage tabFechas2;
        private System.Windows.Forms.Button btnDefectoFechas2;
        private System.Windows.Forms.CheckBox chkInforme;
        private System.Windows.Forms.GroupBox grpInforme;
        private System.Windows.Forms.RadioButton radInformeModificacion;
        private System.Windows.Forms.RadioButton radInformeCreacion;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox chkDiario;
        private System.Windows.Forms.GroupBox grpDiario;
        private System.Windows.Forms.RadioButton radDiarioModificacion;
        private System.Windows.Forms.RadioButton radDiarioCreacion;
        private System.Windows.Forms.CheckBox chkSolicitud;
        private System.Windows.Forms.GroupBox grpSolicitud;
        private System.Windows.Forms.RadioButton radSolicitudModificacion;
        private System.Windows.Forms.RadioButton radSolicitudCreacion;
        private System.Windows.Forms.CheckBox chkSolicitudAct;
        private System.Windows.Forms.CheckBox chkSolicitudRech;
        private System.Windows.Forms.CheckBox chkSolicitudAcept;
        private System.Windows.Forms.GroupBox grpSolicitudAct;
        private System.Windows.Forms.RadioButton radSolicitudActModificacion;
        private System.Windows.Forms.RadioButton radSolicitudActCreacion;
        private System.Windows.Forms.GroupBox grpSolicitudRech;
        private System.Windows.Forms.RadioButton radSolicitudRechModificacion;
        private System.Windows.Forms.RadioButton radSolicitudRechCreacion;
        private System.Windows.Forms.GroupBox grpSolicitudAcept;
        private System.Windows.Forms.RadioButton radSolicitudAceptModificacion;
        private System.Windows.Forms.RadioButton radSolicitudAceptCreacion;
        private System.Windows.Forms.CheckBox chkIntercambio;
        private System.Windows.Forms.GroupBox grpIntercambio;
        private System.Windows.Forms.RadioButton radIntercambioRecordatorio;
        private System.Windows.Forms.RadioButton radIntercambioRecepcion;
        private System.Windows.Forms.RadioButton radIntercambioModificacion;
        private System.Windows.Forms.RadioButton radIntercambioCaducidad;
        private System.Windows.Forms.RadioButton radIntercambioEntregaDiferida;
        private System.Windows.Forms.RadioButton radIntercambioCreacion;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label4;
    }
}