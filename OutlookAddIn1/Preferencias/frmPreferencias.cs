﻿/*    Copyright (C) 2015 - Tinaut1986

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OutlookAddIn1.Preferencias
{
    public partial class frmPreferencias : Form
    {
        private static frmPreferencias instancia = null;

        Utiles.cUtil util = new Utiles.cUtil();

        private frmPreferencias()
        {
            InitializeComponent();
            cPreferencias.Cargar();
        }

        public static frmPreferencias Instancia()
        {
            if (instancia == null)
                instancia = new frmPreferencias();
            return instancia;
        }

        private void btnAplicar_Click(object sender, EventArgs e)
        {
            GuardarCambios();
            Close();
        }

        private void GuardarCambios()
        {
            cPreferencias.instancia.eliminarElementosDirectamente = chkEliminarDirectamente.Checked;
            cPreferencias.instancia.rutaDestinoPST = txtRutaPST.Text;
            cPreferencias.instancia.nombrePST = txtNombreArchivo.Text;
            cPreferencias.instancia.renombrarRaiz = chkRenombrarRaiz.Checked;
            cPreferencias.instancia.eliminarPSTsVaciosTrasArchivar = chkEliminarPSTsVaciosTrasArchiar.Checked;
            cPreferencias.instancia.eliminarCarpetasVaciasDespuesDeArchivar = chkEliminarCarpetasVaciasDespuesDeArchivar.Checked;

            cPreferencias.instancia.archivarPorImportancia = chkArchivarElementosSegunImportancia.Checked ? "1" : "0";
            cPreferencias.instancia.archivarPorImportancia += (chkArchivarElementosNoImportantes.Checked && chkArchivarElementosNoImportantes.Enabled ? "1" : "0");
            cPreferencias.instancia.archivarPorImportancia += (chkArchivarElementosNormales.Checked && chkArchivarElementosNormales.Enabled ? "1" : "0");
            cPreferencias.instancia.archivarPorImportancia += (chkArchivarElementosImportantes.Checked && chkArchivarElementosImportantes.Enabled ? "1" : "0");

            cPreferencias.instancia.archivarElementosNoLeidos = chkArchivarElementosNoLeidos.Checked;
            cPreferencias.instancia.eliminarArchivoPSTSiVacio = chkEliminarArchivoPSTSiVacio.Checked;

            cPreferencias.instancia.archivarCitas = chkCita.Checked;
            cPreferencias.instancia.fechaCitas = chkCita.Checked ? EncontrarTagActivat(grpCita) : 0;
            cPreferencias.instancia.archivarContactos = chkContacto.Checked;
            cPreferencias.instancia.fechaContactos = chkContacto.Checked ? EncontrarTagActivat(grpContacto) : 0;
            cPreferencias.instancia.archivarCorreos = chkCorreo.Checked;
            cPreferencias.instancia.fechaCorreos = chkCorreo.Checked ? EncontrarTagActivat(grpCorreo) : 0;
            cPreferencias.instancia.archivarNotas = chkNota.Checked;
            cPreferencias.instancia.fechaNotas = chkNota.Checked ? EncontrarTagActivat(grpNota) : 0;
            cPreferencias.instancia.archivarReuniones = chkReunion.Checked;
            cPreferencias.instancia.fechaReuniones = chkReunion.Checked ? EncontrarTagActivat(grpReunion) : 0;
            cPreferencias.instancia.archivarTareas = chkTarea.Checked;
            cPreferencias.instancia.fechaTareas = chkTarea.Checked ? EncontrarTagActivat(grpTarea) : 0;
            cPreferencias.instancia.archivarCorreosPublicos = chkCorreoPublico.Checked;
            cPreferencias.instancia.fechaCorreoPublico = chkCorreoPublico.Checked ? EncontrarTagActivat(grpCorreoPublico) : 0;
            cPreferencias.instancia.archivarInformes = chkInforme.Checked;
            cPreferencias.instancia.fechaInforme = chkInforme.Checked ? EncontrarTagActivat(grpInforme) : 0;
            cPreferencias.instancia.archivarDiarios = chkDiario.Checked;
            cPreferencias.instancia.fechaDiario = chkDiario.Checked ? EncontrarTagActivat(grpDiario) : 0;
            cPreferencias.instancia.archivarSolicitudes = chkSolicitud.Checked;
            cPreferencias.instancia.fechaSolicitudes = chkSolicitud.Checked ? EncontrarTagActivat(grpSolicitud) : 0;
            cPreferencias.instancia.archivarSolicitudesAcept = chkSolicitudAcept.Checked;
            cPreferencias.instancia.fechaSolicitudesAcept = chkSolicitudAcept.Checked ? EncontrarTagActivat(grpSolicitudAcept) : 0;
            cPreferencias.instancia.archivarSolicitudesRech = chkSolicitudRech.Checked;
            cPreferencias.instancia.fechaSolicitudesRech = chkSolicitudRech.Checked ? EncontrarTagActivat(grpSolicitudRech) : 0;
            cPreferencias.instancia.archivarSolicitudesAct = chkSolicitudAct.Checked;
            cPreferencias.instancia.fechaSolicitudesAct = chkSolicitudAct.Checked ? EncontrarTagActivat(grpSolicitudAct) : 0;
            cPreferencias.instancia.archivarIntercambio = chkIntercambio.Checked;
            cPreferencias.instancia.fechaIntercambio = chkIntercambio.Checked ? EncontrarTagActivat(grpIntercambio) : 0;

            Preferencias.cPreferencias.instancia.Guardar();
        }

        internal void CargarDatos()
        {
            chkEliminarDirectamente.Checked = cPreferencias.instancia.eliminarElementosDirectamente;
            txtRutaPST.Text = cPreferencias.instancia.rutaDestinoPST;

            txtNombreArchivo.Text = cPreferencias.instancia.nombrePST;
            if (txtNombreArchivo.Text.EndsWith(".pst"))
                txtNombreArchivo.Text = txtNombreArchivo.Text.Substring(0, txtNombreArchivo.Text.Length - 4);

            chkRenombrarRaiz.Checked = cPreferencias.instancia.renombrarRaiz;
            chkEliminarPSTsVaciosTrasArchiar.Checked = cPreferencias.instancia.eliminarPSTsVaciosTrasArchivar;
            chkEliminarCarpetasVaciasDespuesDeArchivar.Checked = cPreferencias.instancia.eliminarCarpetasVaciasDespuesDeArchivar;

            chkArchivarElementosSegunImportancia.Checked = cPreferencias.instancia.archivarPorImportancia[0] == '1';
            chkArchivarElementosImportantes.Checked = cPreferencias.instancia.archivarPorImportancia[3] == '1';
            chkArchivarElementosNormales.Checked = cPreferencias.instancia.archivarPorImportancia[2] == '1';
            chkArchivarElementosNoImportantes.Checked = cPreferencias.instancia.archivarPorImportancia[1] == '1';

            chkArchivarElementosNoLeidos.Checked = cPreferencias.instancia.archivarElementosNoLeidos;
            chkEliminarArchivoPSTSiVacio.Checked = cPreferencias.instancia.eliminarArchivoPSTSiVacio;

            chkCita.Checked = cPreferencias.instancia.archivarCitas;
            if (chkCita.Checked) { ActivarConttrol(grpCita, cPreferencias.instancia.fechaCitas.ToString()); } else { grpCita.Enabled = false; }
            chkContacto.Checked = cPreferencias.instancia.archivarContactos;
            if (chkContacto.Checked) { ActivarConttrol(grpContacto, cPreferencias.instancia.fechaContactos.ToString()); } else { grpContacto.Enabled = false; }
            chkCorreo.Checked = cPreferencias.instancia.archivarCorreos;
            if (chkCorreo.Checked) { ActivarConttrol(grpCorreo, cPreferencias.instancia.fechaCorreos.ToString()); } else { grpCorreo.Enabled = false; }
            chkNota.Checked = cPreferencias.instancia.archivarNotas;
            if (chkNota.Checked) { ActivarConttrol(grpNota, cPreferencias.instancia.fechaNotas.ToString()); } else { grpNota.Enabled = false; }
            chkReunion.Checked = cPreferencias.instancia.archivarReuniones;
            if (chkReunion.Checked) { ActivarConttrol(chkReunion, cPreferencias.instancia.fechaReuniones.ToString()); } else { grpReunion.Enabled = false; }
            chkTarea.Checked = cPreferencias.instancia.archivarTareas;
            if (chkTarea.Checked) { ActivarConttrol(grpTarea, cPreferencias.instancia.fechaTareas.ToString()); } else { grpTarea.Enabled = false; }
            chkCorreoPublico.Checked = cPreferencias.instancia.archivarCorreosPublicos;
            if (chkCorreoPublico.Checked) { ActivarConttrol(grpCorreoPublico, cPreferencias.instancia.fechaCorreoPublico.ToString()); } else { grpCorreoPublico.Enabled = false; }
            chkInforme.Checked = cPreferencias.instancia.archivarInformes;
            if (chkInforme.Checked) { ActivarConttrol(grpInforme, cPreferencias.instancia.fechaInforme.ToString()); } else { grpInforme.Enabled = false; }
            chkDiario.Checked = cPreferencias.instancia.archivarDiarios;
            if (chkDiario.Checked) { ActivarConttrol(grpDiario, cPreferencias.instancia.fechaDiario.ToString()); } else { grpDiario.Enabled = false; }
            chkSolicitud.Checked = cPreferencias.instancia.archivarSolicitudes;
            if (chkSolicitud.Checked) { ActivarConttrol(grpSolicitud, cPreferencias.instancia.fechaSolicitudes.ToString()); } else { grpSolicitud.Enabled = false; }
            chkSolicitudAcept.Checked = cPreferencias.instancia.archivarSolicitudesAcept;
            if (chkSolicitudAcept.Checked) { ActivarConttrol(grpSolicitudAcept, cPreferencias.instancia.fechaSolicitudesAcept.ToString()); } else { grpSolicitudAcept.Enabled = false; }
            chkSolicitudAct.Checked = cPreferencias.instancia.archivarSolicitudesAct;
            if (chkSolicitudAct.Checked) { ActivarConttrol(grpSolicitudAct, cPreferencias.instancia.fechaSolicitudesAct.ToString()); } else { grpSolicitudAct.Enabled = false; }
            chkSolicitudRech.Checked = cPreferencias.instancia.archivarSolicitudesRech;
            if (chkSolicitudRech.Checked) { ActivarConttrol(grpSolicitudRech, cPreferencias.instancia.fechaSolicitudesRech.ToString()); } else { grpSolicitudRech.Enabled = false; }
        }

        private void ActivarConttrol(Control padre, string tag)
        {
            bool salir = false;
            int contador = 0;
            while (contador < padre.Controls.Count && !salir)
            {
                if (padre.Controls[contador].Tag.ToString() == tag && padre.Controls[contador] is RadioButton)
                {
                    ((RadioButton)padre.Controls[contador]).Checked = true;
                    salir = true;
                }
                contador++;
            }
        }

        private int EncontrarTagActivat(Control padre)
        {
            int temporal = 0;
            int contador = 0;
            while (contador < padre.Controls.Count && temporal == 0)
            {
                if (padre.Controls[contador] is RadioButton && ((RadioButton)padre.Controls[contador]).Checked)
                    temporal = Convert.ToInt32(padre.Controls[contador].Tag);
                contador++;
            }
            return temporal;
        }

        private void frmPreferencias_Load(object sender, EventArgs e)
        {
            if (Owner != null)
                Location = new Point(Owner.Location.X + Owner.Width / 2 - Width / 2,
                    Owner.Location.Y + Owner.Height / 2 - Height / 2);
            CargarDatos();
        }

        private void btnAyuda_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://msdn.microsoft.com/es-es/library/8kb3ddd4(v=vs.110).aspx");
        }

        private void txtNombreArchivo_TextChanged(object sender, EventArgs e)
        {
            string expresion = util.TransformarExpresion(txtNombreArchivo.Text);
            if (expresion != "")
            {
                lblResultado.Text = DateTime.Now.ToString(expresion);
                if (!lblResultado.Text.EndsWith(".pst"))
                    lblResultado.Text += ".pst";

                btnAplicar.Enabled = true;
            }
            else
            {
                lblResultado.Text = ".pst";
                btnAplicar.Enabled = false;
            }
        }

        private void chkArchivarElementosSegunImportancia_CheckedChanged(object sender, EventArgs e)
        {
            grpArchivarSegunImportancia.Enabled = chkArchivarElementosSegunImportancia.Checked;
        }

        private void frmPreferencias_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                Close();
        }

        private void chkCorreo_CheckedChanged(object sender, EventArgs e)
        {
            grpCorreo.Enabled = chkCorreo.Checked;
        }

        private void chkCorreoPublico_CheckedChanged(object sender, EventArgs e)
        {
            grpCorreoPublico.Enabled = chkCorreoPublico.Checked;
        }

        private void chkCita_CheckedChanged(object sender, EventArgs e)
        {
            grpCita.Enabled = chkCita.Checked;
        }

        private void chkContacto_CheckedChanged(object sender, EventArgs e)
        {
            grpContacto.Enabled = chkContacto.Checked;
        }

        private void chkReunion_CheckedChanged(object sender, EventArgs e)
        {
            grpReunion.Enabled = chkReunion.Checked;
        }

        private void chkNota_CheckedChanged(object sender, EventArgs e)
        {
            grpNota.Enabled = chkNota.Checked;

        }

        private void chkTarea_CheckedChanged(object sender, EventArgs e)
        {
            grpTarea.Enabled = chkTarea.Checked;
        }

        private void btnDefectoFechas_Click(object sender, EventArgs e)
        {
            chkCorreo.Checked = true;
            radCorreoRecepcion.Checked = true;
            chkCita.Checked = true;
            radCitaInicio.Checked = true;
            chkContacto.Checked = false;
            radContactoCreacion.Checked = true;
            chkReunion.Checked = true;
            radReunionRecepcion.Checked = true;
            chkNota.Checked = false;
            radNotaCreacion.Checked = true;
            chkTarea.Checked = true;
            radTareaCreacion.Checked = true;
            chkCorreoPublico.Checked = true;
            radCorreoPublicoRecepcion.Checked = true;
        }

        private void chkInforme_CheckedChanged(object sender, EventArgs e)
        {
            grpInforme.Enabled = chkInforme.Checked;

        }

        private void frmPreferencias_FormClosed(object sender, FormClosedEventArgs e)
        {
            instancia = null;
        }

        private void btnDefectoFechas2_Click(object sender, EventArgs e)
        {
            chkInforme.Checked= true;
            radInformeCreacion.Checked = true;
            chkDiario.Checked = true;
            radDiarioCreacion.Checked = true;
            chkSolicitud.Checked = true;
            radSolicitudCreacion.Checked = true;
            chkSolicitudAcept.Checked = true;
            radSolicitudAceptCreacion.Checked = true;
            chkSolicitudRech.Checked = true;
            radSolicitudRechCreacion.Checked = true;
            chkSolicitudAct.Checked = true;
            radSolicitudActCreacion.Checked = true;
            chkIntercambio.Checked = true;
            radIntercambioRecepcion.Checked = true;
        }

        private void chkDiario_CheckedChanged(object sender, EventArgs e)
        {
            grpDiario.Enabled = chkDiario.Checked;
        }

        private void chkSolicitud_CheckedChanged(object sender, EventArgs e)
        {
            grpSolicitud.Enabled = chkSolicitud.Checked;
        }

        private void chkSolicitudAcept_CheckedChanged(object sender, EventArgs e)
        {
            grpSolicitudAcept.Enabled = chkSolicitudAcept.Checked;
        }

        private void chkSolicitudRech_CheckedChanged(object sender, EventArgs e)
        {
            grpSolicitudRech.Enabled = chkSolicitudRech.Checked;
        }

        private void chkIntercambio_CheckedChanged(object sender, EventArgs e)
        {
            grpIntercambio.Enabled = chkIntercambio.Checked;
        }

        private void chkSolicitudAct_CheckedChanged(object sender, EventArgs e)
        {
            grpSolicitudAct.Enabled = chkSolicitudAct.Checked;
        }

        private void btnDefectoGenerales_Click(object sender, EventArgs e)
        {
            txtRutaPST.Text = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\ClassifyPST\PSTs Archivados\";
            txtNombreArchivo.Text = "Archivo - <yyyy>";
            chkEliminarDirectamente.Checked = false;
            chkEliminarArchivoPSTSiVacio.Checked = true;
            chkEliminarCarpetasVaciasDespuesDeArchivar.Checked = true;
            chkEliminarPSTsVaciosTrasArchiar.Checked = true;
            chkArchivarElementosNoLeidos.Checked = true;
            chkArchivarElementosSegunImportancia.Checked = false;
        }
    }
}
