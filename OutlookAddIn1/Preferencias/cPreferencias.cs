﻿/*    Copyright (C) 2015 - Tinaut1986

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;

namespace OutlookAddIn1.Preferencias
{
    public class cPreferencias
    {
        public bool detener = false;

        public List<string> pstsParaEliminar = new List<string>();
        public bool eliminarElementosDirectamente = false;
        public bool eliminarCarpetasVaciasDespuesDeArchivar = true;
        public bool renombrarRaiz = true;
        public bool eliminarPSTsVaciosTrasArchivar = true;
        public string archivarPorImportancia = "0000";
        public bool eliminarArchivoPSTSiVacio = true;

        public bool archivarElementosNoLeidos = true;

        public bool archivarCorreos = true;
        public int fechaCorreos = 3;
        public bool archivarCitas = true;
        public int fechaCitas = 4;
        public bool archivarContactos = false;
        public int fechaContactos = 1;
        public bool archivarReuniones = true;
        public int fechaReuniones = 3;
        public bool archivarNotas = false;
        public int fechaNotas = 1;
        public bool archivarTareas = true;
        public int fechaTareas = 1;
        public bool archivarCorreosPublicos = true;
        public int fechaCorreoPublico = 1;
        public bool archivarInformes = true;
        public int fechaInforme = 1;
        public bool archivarDiarios = true;
        public int fechaDiario = 1;
        public bool archivarSolicitudes = true;
        public int fechaSolicitudes = 1;
        public bool archivarSolicitudesAcept = true;
        public int fechaSolicitudesAcept = 1;
        public bool archivarSolicitudesRech = true;
        public int fechaSolicitudesRech = 1;
        public bool archivarSolicitudesAct = true;
        public int fechaSolicitudesAct = 1;
        public bool archivarIntercambio = true;
        public int fechaIntercambio = 1;

        private string _rutaDestinoPST = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\ClassifyPST\PSTs Archivados\";
        public string rutaDestinoPST
        {
            get
            {
                if (!_rutaDestinoPST.EndsWith("\\")) { return _rutaDestinoPST + "\\"; }
                else { return _rutaDestinoPST; }
            }
            set
            {
                if (!value.EndsWith("\\")) { _rutaDestinoPST = value + "\\"; }
                else { _rutaDestinoPST = value; }
            }
        }

        private string _nombrePST = "Archivo - <yyyy>.pst";
        public string nombrePST
        {
            get
            {
                if (!_nombrePST.EndsWith(".pst")) { return _nombrePST + ".pst"; }
                else { return _nombrePST; }
            }
            set
            {
                if (!value.EndsWith(".pst")) { _nombrePST = value + ".pst"; }
                else { _nombrePST = value; }
            }
        }


        [XmlIgnore]
        public const string nombreArchivo = "config.xml";

        [XmlIgnore]
        public static cPreferencias instancia { get; private set; }

        [XmlIgnore]
        public DateTime fechaInicio = new DateTime(DateTime.Now.Year - 1, 1, 1);
        [XmlIgnore]
        public bool archivarFechaInicio = false;
        [XmlIgnore]
        public DateTime fechaFin = new DateTime(DateTime.Now.Year, 1, 1);
        [XmlIgnore]
        public bool archivarFechaFin = true;

        public cPreferencias()
        {
        }


        public static void Defecto()
        {
            cPreferencias.instancia = new cPreferencias();
        }

        public bool Guardar()
        {
            FileStream fStream = null;
            XmlSerializer mySerializer = null;
            bool guardado = false;
            try
            {
                Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\ClassifyPST\");
                // Create an XmlSerializer for the ApplicationSettings type.
                mySerializer = new XmlSerializer(typeof(cPreferencias));
                fStream = new FileStream(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\ClassifyPST\" + cPreferencias.nombreArchivo, FileMode.Create);

                // Serialize this instance of the ApplicationSettings class to the config file.
                mySerializer.Serialize(fStream, this);
                guardado = true;
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }
            finally
            {
                // If the FileStream is open, close it.
                if (fStream != null) fStream.Close();
            }
            frmPrincipal.Instancia().ActualizarPreferenciasActuales();
            return guardado;
        }

        public static void Cargar()
        {
            XmlSerializer mySerializer = null;
            FileStream fStream = null;
            //cPreferencias preferencias = new cPreferencias();

            try
            {
                //// Create an XmlSerializer for the ApplicationSettings type.
                mySerializer = new XmlSerializer(typeof(cPreferencias));

                fStream = new FileStream(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\ClassifyPST\" + cPreferencias.nombreArchivo, FileMode.Open);
                cPreferencias.instancia = (cPreferencias)mySerializer.Deserialize(fStream);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ha habido un problema al cargar el archivo de configuración. Cargando la configuración por defecto.");
                cPreferencias.Defecto();
            }
            finally
            {
                // If the FileStream is open, close it.
                if (fStream != null) fStream.Close();
            }
        }

        /// <summary>
        /// Función que devuelve si el elemento se debe archivar pasandole la importancia del mismo. Si en las preferencias esta indicado
        ///  que no se quiere tener en cuenta la importancia, se devolverá True siempre
        /// </summary>
        /// <param name="">0 -> No importante, 1 -> Normal, 2 -> Importante.</param>
        /// <returns>True si tiene que archivarse o False en caso contrario</returns>
        public bool ArchivarSegunImportancia(int importancia)
        {
            return (archivarPorImportancia[0] == '0' || (archivarPorImportancia[0] == '1' && archivarPorImportancia[importancia + 1] == '1'));

        }

        /// <summary>
        /// Función que añade el PST a la lista de PSTs para eliminar si las preferencias del usuario estan configuradas para hacerlo
        /// </summary>
        /// <param name="ruta">Ruta del PST a eliminar</param>
        public void AnyadirPSTParaEliminar(string ruta)
        {
            if (cPreferencias.instancia.eliminarArchivoPSTSiVacio)
            {
                if (!pstsParaEliminar.Exists(d => d == ruta))
                    pstsParaEliminar.Add(ruta);
                Preferencias.cPreferencias.instancia.Guardar();
            }
        }
    }
}
