﻿/*    Copyright (C) 2015 - Tinaut1986

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

using Microsoft.Office.Interop.Outlook;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutlookAddIn1.Extension
{
    public static class cExtensionTipoElemento
    {
        public static OlDefaultFolders TipoDeCarpeta(this OlItemType tipo)
        {
            OlDefaultFolders tipusCarpeta = new OlDefaultFolders();
            if (tipo == OlItemType.olAppointmentItem)
                tipusCarpeta = OlDefaultFolders.olFolderCalendar;
            else if (tipo == OlItemType.olContactItem)
                tipusCarpeta = OlDefaultFolders.olFolderContacts;
            else if (tipo == OlItemType.olMailItem)
                tipusCarpeta = OlDefaultFolders.olFolderInbox;
            else if (tipo == OlItemType.olNoteItem)
                tipusCarpeta = OlDefaultFolders.olFolderNotes;
            else if (tipo == OlItemType.olTaskItem)
                tipusCarpeta = OlDefaultFolders.olFolderTasks;
            else if (tipo == OlItemType.olJournalItem)
                tipusCarpeta = OlDefaultFolders.olFolderJournal;
            else if (tipo == OlItemType.olPostItem)
                tipusCarpeta = OlDefaultFolders.olFolderInbox;
            else if (tipo == OlItemType.olDistributionListItem)
                tipusCarpeta = OlDefaultFolders.olFolderDeletedItems;
            else if (tipo == OlItemType.olMobileItemMMS)
                tipusCarpeta = OlDefaultFolders.olFolderInbox;
            else if (tipo == OlItemType.olMobileItemSMS)
                tipusCarpeta = OlDefaultFolders.olFolderInbox;

            return tipusCarpeta;
        }
    }
}
