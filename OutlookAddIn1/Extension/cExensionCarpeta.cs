﻿/*    Copyright (C) 2015 - Tinaut1986

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

using Microsoft.Office.Interop.Outlook;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace OutlookAddIn1.Extension
{
    public static class cExtensionCarpeta
    {
        public static int elementoActual;

        /// <summary>
        /// Función que crea una subcarpeta a la carpeta pasada por parametro con el nombre y el tipo indicado, y devuelve la carpeta creada.
        /// </summary>
        /// <param name="carpeta">Carpeta donde se creará la subcarpeta</param>
        /// <param name="nombre">Nombre de la subcarpeta</param>
        /// <param name="tipo">Tipo de la subcarpeta</param>
        /// <returns>Carpeta creada</returns>
        public static MAPIFolder CrearSubCarpeta(this MAPIFolder carpeta, string nombre, OlDefaultFolders tipo, bool forzarCarpetaNueva)
        {
            if (nombre != "")
            {
                int contador = 0, contadorNombreCarpeta = 0;
                bool nombreEncontrado = false;
                string nomFinal = nombre;

                string pattern = @"(.*) \((\d*)\)";
                MatchCollection resultados = Regex.Matches(nombre, pattern, RegexOptions.IgnoreCase);
                if (resultados.Count == 1 && resultados[0].Groups.Count > 2)
                {
                    nombre = resultados[0].Groups[1].Value;
                    contadorNombreCarpeta = Convert.ToInt32(resultados[0].Groups[2].Value);
                }

                while (contador < carpeta.Folders.Count
                    && !nombreEncontrado)
                {
                    contador = 0;
                    while (contador < carpeta.Folders.Count && carpeta.Folders[contador + 1].Name.ToUpper() != nomFinal.ToUpper())
                        contador++;
                    if (contador < carpeta.Folders.Count)
                    {
                        if (carpeta.Folders[contador + 1].DefaultItemType.TipoDeCarpeta() != tipo
                            || forzarCarpetaNueva)
                        {
                            contadorNombreCarpeta++;
                            nomFinal = nombre + " (" + contadorNombreCarpeta + ")";
                        }
                        else nombreEncontrado = true;
                    }
                    else
                        nombreEncontrado = true;
                }
                if (contador >= carpeta.Folders.Count)
                {
                    carpeta.Folders.Add(nomFinal, tipo);
                    carpeta = carpeta.Folders.GetLast();
                }
                else
                    carpeta = carpeta.Folders[contador + 1];
            }
            return carpeta;
        }

        /// <summary>
        /// Función que devuelve el tipo de carpeta (OlDefaultFolders)
        /// </summary>
        /// <param name="carpeta">Carpeta a comprobat</param>
        /// <returns>Tipo de la carpeta (OlDefaultFolders)</returns>
        public static OlDefaultFolders TipoCarpeta(this MAPIFolder carpeta)
        {
            OlDefaultFolders tipusCarpeta = new OlDefaultFolders();
            if (carpeta.DefaultItemType == OlItemType.olAppointmentItem)
                tipusCarpeta = OlDefaultFolders.olFolderCalendar;
            else if (carpeta.DefaultItemType == OlItemType.olContactItem)
                tipusCarpeta = OlDefaultFolders.olFolderContacts;
            else if (carpeta.DefaultItemType == OlItemType.olMailItem)
                tipusCarpeta = OlDefaultFolders.olFolderInbox;
            else if (carpeta.DefaultItemType == OlItemType.olNoteItem)
                tipusCarpeta = OlDefaultFolders.olFolderNotes;
            else if (carpeta.DefaultItemType == OlItemType.olTaskItem)
                tipusCarpeta = OlDefaultFolders.olFolderTasks;
            else if (carpeta.DefaultItemType == OlItemType.olJournalItem)
                tipusCarpeta = OlDefaultFolders.olFolderJournal;

            return tipusCarpeta;
        }

        /// <summary>
        /// Función que archiva los elementos de la carpeta. *Si la carpeta està vacia, ya se elimina aquí segun las preferencias del usuario.*
        /// </summary>
        /// <param name="carpeta">Carpeta a archivar</param>
        public static void Archivar(this MAPIFolder carpeta)
        {
            elementoActual = carpeta.Items.Count;
            while (elementoActual > 0 && !Preferencias.cPreferencias.instancia.detener)
            {
                try
                {
                    Utiles.cElementoOutlook elemento = new Utiles.cElementoOutlook(carpeta.Items[elementoActual]);
                    elemento.Archivar();
                }
                catch (System.Exception ex)
                {
                    string a = ex.ToString();
                }
                elementoActual--;
            }

            if (carpeta.EstaVacia(true))
                carpeta.EliminarCarpeta(true);
        }

        /// <summary>
        /// Función que devuelve True si la carpeta está vacía y False en caso contrario
        /// </summary>
        /// <param name="carpeta">Carpeta a comprobar</param>
        /// <returns>True si la carpeta está vacía y False en caso contrario</returns>
        public static bool EstaVacia(this MAPIFolder carpeta, bool comprobarSubCarpetas)
        {
            bool vacia = carpeta.Items.Count == 0;
            if (vacia && comprobarSubCarpetas)
            {
                int contador = 1;
                while (contador <= carpeta.Folders.Count && vacia)
                {
                    vacia = (carpeta.Folders[contador]).EstaVacia(comprobarSubCarpetas);
                    contador++;
                }
            }
            return vacia;
        }

        /// <summary>
        /// Función que devuelve True si la carpeta está en la papelera de reciclaje y False en caso contrario
        /// </summary>
        /// <param name="carpeta">Carpeta a comprobar</param>
        /// <returns>True si está en la papelera de reciclaje y False en caso contrario</returns>
        public static bool EstaEnPapelera(this MAPIFolder carpeta)
        {
            return (carpeta.EntryID != carpeta.Store.GetRootFolder().EntryID && carpeta.Parent.EntryID == carpeta.Store.GetDefaultFolder(OlDefaultFolders.olFolderDeletedItems).EntryID);
        }

        public static void VaciarCarpeta(this MAPIFolder carpeta, bool archivando)
        {
            elementoActual = carpeta.Items.Count;
            while (elementoActual > 0 && !Preferencias.cPreferencias.instancia.detener)
            {
                try
                {
                    Utiles.cElementoOutlook elemento = new Utiles.cElementoOutlook(carpeta.Items[elementoActual]);
                    elemento.Eliminar();
                }
                catch (System.Exception ex)
                {
                    string a = ex.ToString();
                }
                elementoActual--;
            }

            if (carpeta.EstaVacia(true))
                carpeta.EliminarCarpeta(archivando);
        }

        /// <summary>
        /// Función que elimina una carpeta de un PST.
        /// *Aquí ya se comprueba si se debe eliminar la carpeta segun los parametros y si debe pasar antes por la papelera.
        ///  También se comprueba que no sea la carpeta raiz ni la carpeta de elementos eliminados, ya que estas no pueden eliminarse*
        /// </summary>
        /// <param name="store">PST correspondiente</param>
        /// <param name="carpeta">Carpeta a eliminar</param>
        public static bool EliminarCarpeta(this MAPIFolder carpeta, bool archivando)
        {
            bool eliminada = false;
            if (carpeta.EntryID != carpeta.Store.GetRootFolder().EntryID && carpeta.EntryID != carpeta.Store.GetDefaultFolder(OlDefaultFolders.olFolderDeletedItems).EntryID)
            {
                //Esto es para saber si tiene que eliminarse una vez o dos. La primera va a la bandeja de elementos eliminados, y la segunda se elimina directamente.
                int contador = 0;
                if (Preferencias.cPreferencias.instancia.eliminarElementosDirectamente)
                    contador++;
                if (!carpeta.EstaEnPapelera())
                    contador++;

                try
                {
                    while ((Preferencias.cPreferencias.instancia.eliminarCarpetasVaciasDespuesDeArchivar || !archivando) && contador > 0 & !Preferencias.cPreferencias.instancia.detener)
                    {
                        carpeta.Delete();
                        contador--;
                        eliminada = true;
                    }
                }
                catch (System.Exception ex)
                {

                }
            }
            return eliminada;
        }

        /// <summary>
        /// Función que elimina las subcarpetas si estan vacias.
        /// </summary>
        /// <param name="carpeta">Carpeta a comprobar</param>
        /// <param name="eliminarCarpetaActual">True para eliminar tambien la carpeta actual (en caso de que esté vacia). False para comoprobar solamente las subcarpetas de esta.</param>
        public static void EliminarSubCarpetasVacias(this MAPIFolder carpeta, bool eliminarCarpetaActual, bool archivando)
        {
            bool eliminada = false;

            if (eliminarCarpetaActual && carpeta.EstaVacia(true) && !Preferencias.cPreferencias.instancia.detener)
                eliminada = carpeta.EliminarCarpeta(archivando);

            if (!eliminada)
            {
                int contador = carpeta.Folders.Count;
                while (contador > 0 && !Preferencias.cPreferencias.instancia.detener)
                {
                    (carpeta.Folders[contador]).EliminarSubCarpetasVacias(true, archivando);
                    contador--;
                }
            }
        }

        /// <summary>
        /// Función que mueve todos los elementos de una carpeta a otra
        /// </summary>
        /// <param name="carpeta">Carpeta origen</param>
        /// <param name="destino">Carpeta destino</param>
        public static void MoverElementos(this MAPIFolder carpeta, MAPIFolder destino, DateTime fechaInicio, bool inicio, DateTime fechaFin, bool fin)
        {
            elementoActual = carpeta.Items.Count;
            while (elementoActual > 0 && !Preferencias.cPreferencias.instancia.detener)
            {
                Utiles.cElementoOutlook elemento = new Utiles.cElementoOutlook(carpeta.Items[elementoActual]);
                System.Windows.Forms.Application.DoEvents();
                if ((!inicio || (inicio && elemento.Fecha() >= fechaInicio))
                    && (!fin || (fin && elemento.Fecha() <= fechaFin)))
                {
                    try
                    {
                        elemento.Mover(destino);
                    }
                    catch (System.Exception ex)
                    {
                        string a = ex.ToString();
                    }
                }
                elementoActual--;
            }

            if (carpeta.EstaVacia(true))
                carpeta.EliminarCarpeta(true);
        }
    }
}
