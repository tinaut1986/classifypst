﻿/*    Copyright (C) 2015 - Tinaut1986

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

using Microsoft.Office.Interop.Outlook;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutlookAddIn1.Extension
{
    public static class cExtensionSession
    {
        public static Store BuscarPST(this NameSpace ns, string ruta)
        {
            int contador = 1;
            Store store = null;
            while (contador <= ns.Stores.Count && store == null)
            {
                if (ns.Stores[contador].FilePath == ruta)
                    store = ns.Stores[contador];
                contador++;
            }
            return store;
        }

        /// <summary>
        /// Función que elimina todas las carpetas vacias de la sesión
        /// </summary>
        /// <param name="ns">Sesión a comprobar</param>
        public static void EliminarCarpetasVacias(this NameSpace ns)
        {
            int contador = ns.Stores.Count;
            while (contador > 0)
            {
                (ns.Stores[contador].GetRootFolder()).EliminarSubCarpetasVacias(true, false);

                ns.Stores[contador].QuitarYEliminarSiVacio(false);

                contador--;
            }
        }

        /// <summary>
        /// Función que elimina todos los PSTs que esten vacios
        /// </summary>
        /// <param name="ns"></param>
        public static void EliminarPSTsVacios(this NameSpace ns)
        {
            int contador = ns.Stores.Count;
            while (contador > 0)
            {
                ns.Stores[contador].QuitarYEliminarSiVacio(false);
                contador--;
            }
        }

        public static Store AnyadirPST(this NameSpace ns, string ruta)
        {
            int pstsTotales = ns.Stores.Count;
            Preferencias.cPreferencias.instancia.pstsParaEliminar.Remove(ruta);
            Preferencias.cPreferencias.instancia.Guardar();
            ns.AddStore(ruta);
            if (ns.Stores.Count > pstsTotales)
                return ns.Stores[pstsTotales + 1];
            else
                return ns.BuscarPST(ruta);
        }

        public static void EliminarCarpetasVacias(this NameSpace ns, ref List<KeyValuePair<Store, List<MAPIFolder>>> lista)
        {
            foreach (KeyValuePair<Store, List<MAPIFolder>> par in lista)
            {
                int contador = par.Value.Count - 1;
                while (contador >= 0)
                {
                    try
                    {
                        par.Value[contador].EliminarSubCarpetasVacias(true, false);
                    }
                    catch (System.Exception ex)
                    {

                    }
                    contador--;
                }

                par.Key.QuitarYEliminarSiVacio(false);
            }
        }

        public static void EliminarDiarios(this NameSpace ns, DateTime fechaInicio, bool inicio, DateTime fechaFin, bool fin)
        {
            foreach (Store store in ns.Stores)
                store.EliminarDiario(fechaInicio, inicio, fechaFin, fin);
        }
    }
}
