﻿/*    Copyright (C) 2015 - Tinaut1986

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Outlook;
using pref = OutlookAddIn1.Preferencias.cPreferencias;

namespace OutlookAddIn1.Extension
{
    public static class cExtensionTarea
    {
        /// <summary>
        /// Función que devuelve la fecha de archivado de un tarea.
        /// </summary>
        /// <param name="i">Elemento a comprovar</param>
        /// <returns>Fecha de archivado de un tarea</returns>
        public static DateTime FechaArchivado(this TaskItem i)
        {
            DateTime resultado;
            if (pref.instancia.archivarTareas)
            {
                if (pref.instancia.fechaTareas == 1)
                    resultado = i.CreationTime;
                else if (pref.instancia.fechaTareas == 2)
                    resultado = i.LastModificationTime;
                else
                    resultado = i.ReminderTime;
            }
            else
                throw new System.Exception("La tarea no está marcado para archivarse");

            return resultado;
        }

        /// <summary>
        /// Función que indica si el tarea debe archivarse segun les preferencias del usuario
        /// </summary>
        /// <param name="i">Elemento a comprobar</param>
        /// <returns>True si debe archivarse o false en caso contrario</returns>
        public static bool DebeArchivarse(this TaskItem i)
        {
            bool resultado = true;
            DateTime fechaArchivado = i.FechaArchivado();

            //Comprobar si queremos archivar tareas
            if (resultado && !pref.instancia.archivarTareas)
                resultado = false;
            //Comprobar no archivar el tarea si no está leido y no se quieren archivar los no leidos
            if (resultado && i.UnRead && !pref.instancia.archivarElementosNoLeidos)
                resultado = false;
            //Comprobar no archivar tareas fuera de las fechas solicitadas
            if (resultado && pref.instancia.archivarFechaInicio && pref.instancia.fechaInicio > fechaArchivado)
                resultado = false;
            if (resultado && pref.instancia.archivarFechaFin && pref.instancia.fechaFin < fechaArchivado)
                resultado = false;

            //Comprobar si archivar el tarea segun la importancia del mismo
            if (resultado && !pref.instancia.ArchivarSegunImportancia((int)i.Importance))
                resultado = false;

            return resultado;
        }

        /// <summary>
        /// Función que archiva un tarea en el PST que toque segun las preferencias del usuario y devuelve true si se archiva o false en caso contrario
        /// </summary>
        /// <param name="i">Elemento a archivar</param>
        /// <returns>True si se ha archivado o false en caso contrario</returns>
        public static bool Archivar(this TaskItem i)
        {
            bool archivado = true;
            Utiles.cUtil util = new Utiles.cUtil();
            string ruta = "", nombreArchivo = "";
            Store store;
            if (i.DebeArchivarse())
            {
                DateTime fechaArchivado = i.FechaArchivado();
                nombreArchivo = util.CrearNombre(fechaArchivado);
                ruta = pref.instancia.rutaDestinoPST + nombreArchivo;
                Globals.ThisAddIn.Application.Session.AddStore(ruta);

                //Encontrar el PST al que moveremos el tarea, ya sea el PST nuevo o existente
                store = Globals.ThisAddIn.Application.Session.AnyadirPST(ruta);

                //Cambiar el nombre de la carpeta raiz si está configurado en las preferencias
                if (pref.instancia.renombrarRaiz) { store.GetRootFolder().Name = nombreArchivo.Replace(".pst", ""); }

                MAPIFolder destino = store.CrearRuta((MAPIFolder)i.Parent, i.TipoCarpeta());
                try
                {
                    if (destino.StoreID != i.Parent.StoreID)
                        i.Move(destino);
                }
                catch (System.Exception ex)
                {
                    archivado = false;
                }
            }
            return archivado;
        }

        /// <summary>
        /// Función que devuelve el tipo de carpeta que toca para este elemento
        /// </summary>
        /// <param name="i">Elemento a comprobar</param>
        /// <returns>Tipo de carpeta que toca al elemento</returns>
        public static OlDefaultFolders TipoCarpeta(this TaskItem i)
        {
            return OlDefaultFolders.olFolderTasks;
        }

        /// <summary>
        /// Función que devuelve True si el elemento está en la papelera de reciclaje y False en caso contrario
        /// </summary>
        /// <param name="i">Elemento a comprobar</param>
        /// <returns>True si está en la papelera de reciclaje y False en caso contrario</returns>
        public static bool EstaEnPapelera(this TaskItem i)
        {
            return (i.EntryID != ((MAPIFolder)i.Parent).Store.GetRootFolder().EntryID
                && i.Parent.EntryID == ((MAPIFolder)i.Parent).Store.GetDefaultFolder(OlDefaultFolders.olFolderDeletedItems).EntryID);
        }

        /// <summary>
        /// Función que elimina el elemento
        /// </summary>
        /// <param name="i">Elemento a eliminar</param>
        /// <returns>True si se ha eliminado. Si no se elimina, devolverá excepción</returns>
        public static bool Eliminar(this TaskItem i)
        {
            int contador = 0;
            if (Preferencias.cPreferencias.instancia.eliminarElementosDirectamente) contador++;
            if (!i.EstaEnPapelera()) { contador++; }
            while (contador > 0)
            {
                i.Delete();
                contador--;
            }
            return true;
        }
    }
}
