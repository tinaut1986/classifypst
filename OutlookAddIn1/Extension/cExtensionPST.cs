﻿/*    Copyright (C) 2015 - Tinaut1986

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

using Microsoft.Office.Interop.Outlook;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutlookAddIn1.Extension
{
    public static class cExtensionPST
    {
        public static MAPIFolder carpetaActual;

        public static MAPIFolder CrearRuta(this Store store, MAPIFolder carpeta, OlDefaultFolders tipoDeCarpeta)
        {
            MAPIFolder carpetaDestino = store.GetRootFolder();
            if (store.StoreID != carpeta.StoreID)
            {
                //MAPIFolder carpetaOrigen = carpeta.Store.GetRootFolder();
                MAPIFolder carpetaOrigen = Globals.ThisAddIn.Application.Session.GetStoreFromID(carpeta.StoreID).GetRootFolder();

                carpetaDestino = store.GetRootFolder();
                string ruta = carpeta.FullFolderPath.Replace("%2F", "/");
                ruta = ruta.Replace("\\\\" + carpetaOrigen.Name, "\\" + carpetaDestino.Name);

                string[] carpetasPorCrear = ruta.Split('\\');
                int contadorCarpetasPorCrear = 0;
                bool carpetaRaizYaRecorrida = false;

                while (contadorCarpetasPorCrear < carpetasPorCrear.Length)
                {
                    if (carpetasPorCrear[contadorCarpetasPorCrear] != "")
                    {
                        if (carpetaRaizYaRecorrida)
                        {
                            OlDefaultFolders TipoCarpetaDestino;
                            if (contadorCarpetasPorCrear < carpetasPorCrear.Length - 1)
                            {
                                int contadorCarpetasOrigen = 1;
                                while (contadorCarpetasOrigen <= carpetaOrigen.Folders.Count
                                    && carpetaOrigen.Folders[contadorCarpetasOrigen].Name.ToUpper() != carpetasPorCrear[contadorCarpetasPorCrear].ToUpper())
                                    contadorCarpetasOrigen++;

                                carpetaOrigen = carpetaOrigen.Folders[contadorCarpetasOrigen];
                                if (carpetaOrigen.Items.Count > 0)
                                {
                                    Utiles.cElementoOutlook elemento = new Utiles.cElementoOutlook(carpetaOrigen.Items[carpetaOrigen.Items.Count]);
                                    TipoCarpetaDestino = elemento.TipoCarpeta();
                                }
                                else
                                    TipoCarpetaDestino = carpetaOrigen.TipoCarpeta();
                            }
                            else
                                TipoCarpetaDestino = tipoDeCarpeta;

                            carpetaDestino = carpetaDestino.CrearSubCarpeta(carpetasPorCrear[contadorCarpetasPorCrear], TipoCarpetaDestino, false);
                        }
                        else
                            carpetaRaizYaRecorrida = true;
                    }
                    contadorCarpetasPorCrear++;
                }
            }
            else
                carpetaDestino = carpeta;
            return carpetaDestino;
        }

        public static bool EstaVacio(this Store store)
        {
            return (store.GetRootFolder()).EstaVacia(true);
        }

        /// <summary>
        /// Función que elimina el almacen de la sesión y lo marca para eliminar automaticamente si las preferencias del usuario lo indican.
        /// Si quitarAunqueNoEsteVacio es True, se quitara de la sesión aunque tenga elementos (No se eliminará el archivo)
        /// </summary>
        /// <param name="store">Store a eliminar</param>
        /// <param name="quitarAunqueNoEsteVacio">True si se quiere quitar de la sesion aunque no esté vacio (No se eliminará el archivo).
        /// False si solo se quiere quitar en caso de estar vacio (Se eliminará el archivo).</param>
        public static void QuitarYEliminarSiVacio(this Store store, bool quitarAunqueNoEsteVacio)
        {
            bool vacio = store.EstaVacio();
            if (store.StoreID != Globals.ThisAddIn.Application.Session.DefaultStore.StoreID)
            {
                if (quitarAunqueNoEsteVacio || (!quitarAunqueNoEsteVacio && vacio))
                {
                    if (Preferencias.cPreferencias.instancia.eliminarArchivoPSTSiVacio && vacio)
                    {
                        Preferencias.cPreferencias.instancia.AnyadirPSTParaEliminar(store.FilePath);
                        Globals.Ribbons.Ribbon1.EncenderCronoEliminarPSTs();
                    }
                    Globals.ThisAddIn.Application.Session.RemoveStore(store.GetRootFolder());
                }
            }
        }

        public static void EliminarCarpetasVacias(this Store store)
        {
            (store.GetRootFolder()).EliminarSubCarpetasVacias(true, false);
        }

        public static void EliminarDiario(this Store store, DateTime fechaInicio, bool inicio, DateTime fechaFin, bool fin)
        {
            carpetaActual = store.GetDefaultFolder(OlDefaultFolders.olFolderJournal).CrearSubCarpeta("Eliminados-Diario", OlDefaultFolders.olFolderJournal, true);
            store.GetDefaultFolder(OlDefaultFolders.olFolderJournal).MoverElementos(carpetaActual, fechaInicio, inicio, fechaFin, fin);
            carpetaActual.EliminarCarpeta(false);
        }
    }
}
